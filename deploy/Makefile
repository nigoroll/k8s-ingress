# Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
# All rights reserved
#
# Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# GNU make is required.

mkpath := $(abspath $(lastword $(MAKEFILE_LIST)))
mkdir := $(dir $(mkpath))

GEN_SECRET=head -c32 /dev/urandom | base64 | tr +/ -_

# For the klarlack image: make VARNISH=klarlack ...
ifndef VARNISH
	VARNISH=varnish
endif

CI_REPO_PFX=registry.gitlab.com/uplex/varnish/k8s-ingress/varnish-ingress

# For tests using the local docker registry: make TEST=local ...
# For tests using images from the CI pipeline: make TEST=ci ...
ifeq ($(TEST),local)
	CONTROLLER_IMAGE=varnish-ingress/controller
	CONTROLLER_TAG=latest
	VARNISH_IMAGE=varnish-ingress/$(VARNISH)
	VARNISH_TAG=latest
	HAPROXY_IMAGE=varnish-ingress/haproxy
	HAPROXY_TAG=latest
else ifeq ($(TEST),ci)
	CONTROLLER_IMAGE=$(CI_REPO_PFX)/controller
	CONTROLLER_TAG=master
	VARNISH_IMAGE=$(CI_REPO_PFX)/$(VARNISH)
	VARNISH_TAG=master
	HAPROXY_IMAGE=$(CI_REPO_PFX)/haproxy
	HAPROXY_TAG=master
endif

# If not specified, pull the latest "official" images from dockerhub.
LATEST=0.1.0
ifndef CONTROLLER_IMAGE
	CONTROLLER_IMAGE=uplex/viking-controller
endif
ifndef CONTROLLER_TAG
	CONTROLLER_TAG=$(LATEST)
endif
ifndef VARNISH_IMAGE
	VARNISH_IMAGE=uplex/viking-$(VARNISH)
endif
ifndef VARNISH_TAG
	VARNISH_TAG=$(LATEST)
endif
ifndef HAPROXY_IMAGE
	HAPROXY_IMAGE=uplex/viking-haproxy
endif
ifndef HAPROXY_TAG
	HAPROXY_TAG=$(LATEST)
endif

ifndef WAIT_TIMEOUT
	WAIT_TIMEOUT=2m
endif

all: deploy

deploy-controller-helm:
	@helm install viking-controller $(mkdir)/../charts/viking-controller \
		--values values-controller.yaml --namespace kube-system \
		--set vikingController.image.repository=$(CONTROLLER_IMAGE) \
		--set vikingController.image.tag=$(CONTROLLER_TAG) \
		--set vikingController.varnishImpl=$(VARNISH)

deploy-controller-kubectl:
	@kubectl apply -f serviceaccount-controller.yaml
	@kubectl apply -f rbac-controller.yaml
	@kubectl apply -f ingress-class.yaml
	@kubectl apply -f varnishcfg-crd.yaml
	@kubectl apply -f backendcfg-crd.yaml
	@kubectl apply -f templatecfg-crd.yaml
ifeq ($(VARNISH),klarlack)
	@kubectl apply -f controller_klarlack.yaml
else
	@kubectl apply -f controller.yaml
endif

deploy-controller:

ifeq ($(DEPLOY),kubectl)
deploy-controller: deploy-controller-kubectl
else
deploy-controller: deploy-controller-helm
endif

deploy-service-helm:
	@helm install viking-service $(mkdir)/../charts/viking-service \
		--values values-varnish.yaml \
		--set vikingService.secrets.admin=$(shell $(GEN_SECRET)) \
		--set vikingService.secrets.dataplaneapi=$(shell $(GEN_SECRET)) \
		--set vikingService.varnish.image.repository=$(VARNISH_IMAGE) \
		--set vikingService.varnish.image.tag=$(VARNISH_TAG) \
		--set vikingService.haproxy.image.repository=$(HAPROXY_IMAGE) \
		--set vikingService.haproxy.image.tag=$(HAPROXY_TAG)

deploy-service-kubectl:
	@kubectl apply -f serviceaccount-varnish.yaml
	@kubectl apply -f rbac-varnish.yaml
	@kubectl apply -f adm-secret.yaml
ifeq ($(VARNISH),klarlack)
	@kubectl apply -f varnish_klarlack.yaml
else
	@kubectl apply -f varnish.yaml
endif
	@kubectl apply -f admin-svc.yaml
	@kubectl apply -f service.yaml

deploy-service:

ifeq ($(DEPLOY),kubectl)
deploy-service: deploy-service-kubectl
else
deploy-service: deploy-service-helm
endif

# TESTOPTS are passed to varnishtest, e.g.: make TESTOPTS=-v verify
verify:
	$(mkdir)/verify.sh

undeploy-service-helm:
	@helm uninstall viking-service
	@echo Waiting for viking-service Pods to be deleted
	@kubectl wait pod --timeout=$(WAIT_TIMEOUT) \
		-l app.kubernetes.io/name=varnish-ingress --for=delete

undeploy-service-kubectl:
	@kubectl delete -f rbac-varnish.yaml
	@kubectl delete -f serviceaccount-varnish.yaml
	@kubectl delete -f service.yaml
	@kubectl delete -f admin-svc.yaml
ifeq ($(VARNISH),klarlack)
	@kubectl delete -f varnish_klarlack.yaml
else
	@kubectl delete -f varnish.yaml
endif
	@kubectl delete -f adm-secret.yaml
	@echo Waiting for viking-service Pods to be deleted
	@kubectl wait pod --timeout=$(WAIT_TIMEOUT) \
		-l app=varnish-ingress --for=delete

undeploy-service:

ifeq ($(DEPLOY),kubectl)
undeploy-service: undeploy-service-kubectl
else
undeploy-service: undeploy-service-helm
endif

undeploy-controller-helm:
	@helm uninstall viking-controller --namespace kube-system
	@kubectl delete crd backendconfigs.ingress.varnish-cache.org
	@kubectl delete crd varnishconfigs.ingress.varnish-cache.org
	@kubectl delete crd templateconfigs.ingress.varnish-cache.org
	@echo Waiting for the viking-controller Pod to be deleted
	@kubectl wait pod --timeout=$(WAIT_TIMEOUT) -n kube-system \
		-l app.kubernetes.io/name=viking-controller --for=delete

undeploy-controller-kubectl:
ifeq ($(VARNISH),klarlack)
	@kubectl delete -f controller_klarlack.yaml
else
	@kubectl delete -f controller.yaml
endif
	@kubectl delete -f templatecfg-crd.yaml
	@kubectl delete -f backendcfg-crd.yaml
	@kubectl delete -f varnishcfg-crd.yaml
	@kubectl delete -f ingress-class.yaml
	@kubectl delete -f rbac-controller.yaml
	@kubectl delete -f serviceaccount-controller.yaml
	@echo Waiting for the viking-controller Pod to be deleted
	@kubectl wait pod --timeout=$(WAIT_TIMEOUT) -n kube-system \
		-l app=varnish-ingress-controller --for=delete

undeploy-controller:

ifeq ($(DEPLOY),kubectl)
undeploy-controller: undeploy-controller-kubectl
else
undeploy-controller: undeploy-controller-helm
endif

deploy: deploy-controller deploy-service

undeploy: undeploy-service undeploy-controller

.PHONY: all $(MAKECMDGOALS)
