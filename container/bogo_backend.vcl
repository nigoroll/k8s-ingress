# Bogus backend, to which no request is ever sent.
# - Sentinel that no backend was determined after a request has been
#   evaluated according to IngressRules.
# - Defined when the Ingress is not ready (not currently implementing
#   any IngressSpec), so that Varnish doesn't complain about no
#   backend definition.
backend vk8s_notfound None;
