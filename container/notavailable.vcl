vcl 4.1;

include "bogo_backend.vcl";

# Send a synthetic response with status 503 to every request.
# Used for the configured check and regular traffic when Varnish is
# not configured to implement an Ingress.
sub vcl_recv {
	return(synth(503));
}

# Empty response body.
sub vcl_synth {
	return (deliver);
}
