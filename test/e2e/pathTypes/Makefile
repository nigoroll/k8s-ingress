# Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
# All rights reserved
#
# Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# GNU make is required.

mkpath := $(abspath $(lastword $(MAKEFILE_LIST)))
mkdir := $(dir $(mkpath))

CHARTDIR=$(mkdir)/../../../charts
TESTDIR=$(mkdir)/../..

all: deploy

no-kubectl:
	$(warning This test runs with helm only)
	@true

ifeq ($(DEPLOY),kubectl)
deploy verify wait uninstall undeploy: no-kubectl
else

deploy:
	@helm install viking-ingress-pathtypes $(CHARTDIR)/viking-test-app \
		--values values.yaml

# TESTOPTS are passed to varnishtest, e.g.: make TESTOPTS=-v verify
verify:
	$(mkdir)/verify.sh pathtypes.vtc

wait:
	@echo Waiting until varnish-ingress Pods are not configured for Ingress
	$(TESTDIR)/wait.sh app=varnish-ingress

uninstall:
	@helm uninstall viking-ingress-pathtypes

undeploy: uninstall wait
endif

.PHONY: all $(MAKECMDGOALS)
