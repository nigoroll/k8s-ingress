# Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
# All rights reserved
#
# Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# GNU make is required.

mkpath := $(abspath $(lastword $(MAKEFILE_LIST)))
mkdir := $(dir $(mkpath))

CHARTDIR=$(mkdir)/../../../charts
TESTDIR=$(mkdir)/../..

all: deploy

deploy-helm:
	@helm install viking-ingress-delete-secret $(CHARTDIR)/viking-test-app \
		--values values.yaml
	@kubectl create -f other-tls-secret.yaml
	@kubectl create -f other-ingress.yaml

deploy-kubectl:
	@kubectl create -f cafe-tls-secret.yaml
	@kubectl create -f cafe.yaml
	@kubectl create -f cafe-ingress.yaml
	@kubectl create -f other-tls-secret.yaml
	@kubectl create -f other-ingress.yaml

# TESTOPTS are passed to varnishtest, e.g.: make TESTOPTS=-v verify
verify:
	$(mkdir)/verify.sh

wait:
	$(TESTDIR)/wait.sh app=varnish-ingress

uninstall-kubectl:
	@kubectl delete -f other-ingress.yaml
	@kubectl delete -f cafe-ingress.yaml
	@kubectl delete -f cafe.yaml
	@kubectl delete -f cafe-tls-secret.yaml

uninstall-helm:
	@kubectl delete -f other-ingress.yaml
	@helm uninstall viking-ingress-delete-secret

undeploy-kubectl: uninstall-kubectl wait

undeploy-helm: uninstall-helm wait

ifeq ($(DEPLOY),kubectl)
deploy: deploy-kubectl
undeploy: undeploy-kubectl
else
deploy: deploy-helm
undeploy: undeploy-helm
endif

.PHONY: all $(MAKECMDGOALS)
