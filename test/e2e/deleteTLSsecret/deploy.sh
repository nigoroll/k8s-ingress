#! /bin/bash -ex

kubectl create -f cafe-tls-secret.yaml

kubectl create -f cafe.yaml

kubectl create -f cafe-ingress.yaml

kubectl create -f other-tls-secret.yaml

kubectl create -f other-ingress.yaml
