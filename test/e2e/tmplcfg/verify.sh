#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../utils.sh

LOCALPORT=${LOCALPORT:-8888}

wait_until_ready app=varnish-ingress-controller dev
wait_until_ready app=varnish-ingress dev
wait_until_configured app=varnish-ingress dev

kubectl port-forward -n dev svc/varnish-ingress ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
