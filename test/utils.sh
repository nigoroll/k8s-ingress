#! /bin/bash

function wait_for_pod_status {
    local label="$1"
    local ns=${2-default}
    local timeout=${3-2m}
    local for=${4-condition=Ready}

    kubectl wait pod -n ${ns} --timeout=${timeout} -l ${label} --for=${for}
}

function wait_until_ready {
    wait_for_pod_status "$@"
}

function wait_until_deleted {
    local label="$1"
    local ns=${2-default}
    local timeout=${3-2m}

    wait_for_pod_status ${label} ${ns} ${timeout} "delete"
}

function wait_for_port {
    local port="$1"
    local timeout=${2-120}

    set +e
    N=0
    while true; do
        sleep 1
        cat < /dev/null > /dev/tcp/localhost/${port}
        if [ $? -eq 0 ]; then
            break
        fi
        if [ $N -ge 120 ]; then
            echo "Timed out waiting for listener at port ${port}"
            exit 1
        fi
        N=$(( N + 1 ))
    done
    set -e
}

function wait_for_config_status {
    local label="$1"
    local ns=${2-default}
    local timeout=${3-120}
    local port=${4-8000}
    local expect=${5-200}

    pods=( $(kubectl get pods -n ${ns} -l ${label} --no-headers -o custom-columns=":metadata.name") )

    for pod in "${pods[@]}"
    do
        kubectl port-forward -n ${ns} pod/${pod} ${port}:${port} &
        local PODPID=$!

        wait_for_port ${port} ${timeout}

        local N=0
        while true; do
            sleep 1
            local status=$(curl -s -o /dev/null -w "%{http_code}" -I http://localhost:${port})
            if [ ${status} -eq ${expect} ]; then
                break
            fi
            if [ $N -ge ${timeout} ]; then
                echo "Timed out waiting for status ${expect} from pod ${pod}"
                kill $PODPID
                exit 1
            fi
            N=$(( N + 1 ))
        done
        kill $PODPID
    done
}

function wait_until_configured {
    wait_for_config_status "$@"
}

function wait_until_not_configured {
    local label="$1"
    local ns=${2-default}
    local timeout=${3-120}
    local port=${4-8000}

    wait_for_config_status ${label} ${ns} ${timeout} ${port} '503'
}

# kubectl scale --timeout is evidently buggy in some versions, so we
# wait until kubectl get pods outputs the expected number of replicas.
function wait_for_replica_count {
    local label="$1"
    local replicas="$2"
    local ns=${3-default}
    local timeout=${4-120}

    local N=0
    while true; do
        local npods=$(kubectl get pods -n ${ns} -l ${label} --no-headers | wc -l)
        if [ $? -ne 0 ]; then
            exit 1
        fi
        if [ ${npods} -eq ${replicas} ]; then
            break
        fi
        if [ $N -ge ${timeout} ]; then
            echo "Timed out waiting for $replicas replicas"
            exit 1
        fi
        N=$(( N + 1 ))
        sleep 1
    done
}
