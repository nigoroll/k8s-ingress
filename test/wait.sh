#! /bin/bash -ex

if [ -z "$1" ]; then
    echo "Usage: $0 label [namespace] [timeout]"
    exit 1
fi

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/utils.sh

wait_until_not_configured "$@"
