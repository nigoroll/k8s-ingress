#! /bin/bash -ex

function undeploy_all {
    cd ${MYPATH}/../deploy
    make undeploy
}

MYPATH="$( cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 ; pwd -P )"
source ${MYPATH}/utils.sh
trap undeploy_all EXIT

export TESTOPTS=-v

echo Test deployment of controller and Varnish-as-Ingress
cd ${MYPATH}/../deploy/
make deploy verify

echo "Hello, world!" example
cd ${MYPATH}/../examples/hello/
make deploy verify undeploy

echo "Hello, world!" example with TLS offload
cd ${MYPATH}/../examples/tls/hello
make deploy verify undeploy

echo TLS offload example with multiple certificates distinguished by SNI
cd ${MYPATH}/../examples/tls/sni
make deploy verify undeploy

echo TLS onload
cd ${MYPATH}/../examples/tls/onload
make deploy verify undeploy

echo Single namespace example
cd ${MYPATH}/../examples/namespace/
make deploy verify undeploy

echo Varnish Pod template with CLI args example
cd ${MYPATH}/../examples/varnish_pod_template/
make EXAMPLE=cli-args deploy verify undeploy

echo Varnish Pod template with PROXY protocol example
make EXAMPLE=proxy deploy verify undeploy

echo Varnish Pod template with env settings example
make EXAMPLE=env deploy verify undeploy

echo Varnish Pod template with containers running as nonroot example
make EXAMPLE=nonroot deploy verify undeploy

echo Cluster-wide Ingress example
cd ${MYPATH}/../examples/architectures/clusterwide/
make deploy verify undeploy

echo Example with a cluster-wide Ingress and a namespace-specific Ingress
cd ${MYPATH}/../examples/architectures/cluster-and-ns-wide/
make deploy verify undeploy

echo Multiple Varnish Services in a namespace example
cd ${MYPATH}/../examples/architectures/multi-varnish-ns/
make deploy verify undeploy

echo Multiple Ingress controllers example
cd ${MYPATH}/../examples/architectures/multi-controller/
make deploy verify undeploy

echo Custom VCL example
cd ${MYPATH}/../examples/custom-vcl/
make deploy verify undeploy

echo Self-sharding cluster example
cd ${MYPATH}/../examples/self-sharding/
make EXAMPLE=self-sharding deploy verify undeploy

echo Primary-only self-sharding cluster example
make EXAMPLE=primary-only deploy verify undeploy

echo Self-sharding by digest example
make EXAMPLE=shard-by-digest deploy verify undeploy

echo Self-sharding by URL example
make EXAMPLE=shard-by-url deploy verify undeploy

echo Self-sharding by key example
make EXAMPLE=shard-by-key deploy verify undeploy

echo Self-sharding by cookie example
make EXAMPLE=shard-by-cookie deploy verify-cookie undeploy

echo Primary-only self-sharding by client.identity as key
make EXAMPLE=primary-only-by-clientid deploy verify undeploy

echo Self-sharding under conditions example
make EXAMPLE=shard-conditions deploy verify undeploy

echo Self-sharding with 1 replica
make EXAMPLE=self-sharding deploy verify
kubectl scale --replicas=1 --timeout=2m deploy/varnish-ingress
wait_for_replica_count app=varnish-ingress 1
make EXAMPLE=self-sharding verify
kubectl scale --replicas=2 --timeout=2m deploy/varnish-ingress
wait_for_replica_count app=varnish-ingress 2
make EXAMPLE=self-sharding verify undeploy

echo Basic Authentication example
cd ${MYPATH}/../examples/authentication/
make EXAMPLE=basic-auth deploy verify undeploy

echo Proxy Authentication example
make EXAMPLE=proxy-auth deploy verify undeploy

echo Combined ACL and Basic Authentication example
make EXAMPLE=acl-or-auth deploy verify undeploy

echo Access control list examples
cd ${MYPATH}/../examples/acl/
make deploy verify undeploy

echo Rewrite rule examples
cd ${MYPATH}/../examples/rewrite/
make deploy verify undeploy

echo Request disposition examples: re-implementing default vcl_recv
cd ${MYPATH}/../examples/req-disposition/
make EXAMPLE=builtin deploy verify undeploy

echo Request disposition examples: alternative re-implemention of default vcl_recv
make EXAMPLE=alt-builtin deploy verify undeploy

echo Request disposition examples: pass on certain cookies, lookup on all others
make EXAMPLE=cookie-pass deploy verify undeploy

echo Request disposition examples: cacheability rules by URL path pattern
make EXAMPLE=cacaheability deploy verify undeploy

echo Request disposition examples: URL white- and blacklisting
make EXAMPLE=url-whitelist deploy verify undeploy

echo Request disposition examples: PURGE method
make EXAMPLE=purge deploy verify undeploy

echo BackendConfig example
cd ${MYPATH}/../examples/backend-config/
make deploy verify undeploy

echo -sfile cache storage with StatefulSet example
cd ${MYPATH}/../examples/file-cache/
make deploy verify undeploy

echo Service types NodePort and LoadBalancer examples
cd ${MYPATH}/../examples/svctypes/
make EXAMPLE=nodeport deploy verify undeploy
make EXAMPLE=lb deploy verify undeploy

echo Varieties of Ingress
cd ${MYPATH}/e2e/ingresses
make EXAMPLE=fanout-nohost deploy verify undeploy
make EXAMPLE=vhost-nohost deploy verify undeploy
make EXAMPLE=wildcard-host deploy verify undeploy

echo Ingress pathType field
cd ${MYPATH}/e2e/pathTypes
make deploy verify undeploy

echo Ignore deletion of TLS Secrets for non-viking Ingresses
cd ${MYPATH}/e2e/deleteTLSsecret
make deploy verify undeploy

echo Update TLS Secrets when their contents are changed
cd ${MYPATH}/e2e/updateTLSsecret
make deploy verify undeploy

echo Examples for devmode and the TemplateConfig CRD
cd ${MYPATH}/e2e/tmplcfg
make deploy verify undeploy

echo probeConfig fields in the viking-service helm chart, liveness and readiness
cd ${MYPATH}/e2e/probeCfg
make deploy verify undeploy

echo haproxy global timeouts
cd ${MYPATH}/e2e/haproxyTimeout
make deploy verify undeploy

echo Example of an ExternalName Service as an Ingress backend
cd ${MYPATH}/../examples/externalname/
make deploy verify undeploy

# Wait for prior versions of the IngressBackends from the previous
# test to delete.
set +e
wait_until_deleted app=coffee,example=externalname
wait_until_deleted app=tea,example=externalname
set -e

# Now re-deploy and verify again -- the Service is assigned a new IP
# address, verify that the new address is resolved.
# Since the Service does not exist for a brief time, DNS lookups get
# negative results during this time. We allow verification failures
# for up to 2 minutes.
make deploy
N=0
while true; do
    if ! make verify; then
        if [ $N -gt 120 ]; then
            exit 1
        fi
        sleep 10
        N=$(( N + 10 ))
        continue
    else
        break
    fi
done
make undeploy

exit 0
