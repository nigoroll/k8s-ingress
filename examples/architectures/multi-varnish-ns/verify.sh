#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../../test/utils.sh

COFFEEPORT=${COFFEEPORT:-8888}
TEAPORT=${TEAPORT:-9999}

wait_until_ready app=varnish-ingress cafe
wait_until_configured app=varnish-ingress cafe

kubectl port-forward -n cafe svc/varnish-coffee ${COFFEEPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT

kubectl port-forward -n cafe svc/varnish-tea ${TEAPORT}:80 >/dev/null &

wait_for_port ${COFFEEPORT}
wait_for_port ${TEAPORT}

varnishtest ${TESTOPTS} -Dcoffeeport=${COFFEEPORT} -Dteaport=${TEAPORT} cafe.vtc

# Parse the controller log for these lines
# Ingresses implemented by Varnish Service cafe/varnish-tea: [cafe/tea-ingress]
# Ingresses implemented by Varnish Service cafe/varnish-coffee: [cafe/coffee-ingress]

# Get the name of the controller Pod
CTLPOD=$(kubectl get pods -n kube-system -l app=varnish-ingress-controller -o jsonpath={.items[0].metadata.name})

# Match the logs
kubectl logs -n kube-system $CTLPOD | grep -q 'Ingresses implemented by Varnish Service cafe/varnish-coffee-admin: \[cafe/coffee-ingress\]'
kubectl logs -n kube-system $CTLPOD | grep -q 'Ingresses implemented by Varnish Service cafe/varnish-tea-admin: \[cafe/tea-ingress\]'
