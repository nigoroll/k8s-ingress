#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../test/utils.sh

IP=${IP:-127.0.0.1}
LOCALPORT=${LOCALPORT:-8888}

svctypes=$(kubectl get pods -l app=varnish-ingress,example=svctypes)
if [[ -n ${svctypes} ]]; then
    wait_until_ready app=varnish-ingress,example=svctypes
    wait_until_configured app=varnish-ingress,example=svctypes
else
    wait_until_ready app=varnish-ingress
    wait_until_configured app=varnish-ingress
fi

NODEPORT=$(kubectl get svc svctypes-example -o jsonpath='{.spec.ports[0].nodePort}')
EXTERNALIP=$(kubectl get svc svctypes-example -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
HTTPPORT=$(kubectl get svc svctypes-example -o jsonpath='{.spec.ports[0].port}')

if [[ -n ${CLUSTERIP} ]] && [[ -n ${NODEPORT} ]]; then
    # If CLUSTERIP is set, we can test the NodePort example using the
    # node IP and nodePort.
    # For minikube: CLUSTERIP=$(minikube ip)
    IP=${CLUSTERIP}
    PORT=${NODEPORT}
elif [[ -n ${EXTERNALIP} ]] && [[ -n ${HTTPPORT} ]]; then
    # If an external IP was found, we can test the LoadBalancer
    # example using the external IP and HTTP port.
    IP=${EXTERNALIP}
    PORT=${HTTPPORT}
else
    # Otherwise use port forwarding. This just forwards connections to
    # the ClusterIP (so it doesn't really test the Service types).
    PORT=${LOCALPORT}
    kubectl port-forward svc/svctypes-example ${PORT}:80 >/dev/null &
    trap 'kill $(jobs -p)' EXIT
fi

if [[ "${IP}" == "${CLUSTERIP}" ]] || [[ "${IP}" == "${EXTERNALIP}" ]]; then
    ip=$(kubectl get ing cafe-ingress-varnish -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
    if [[ -z ${ip} ]]; then
        echo "IP address not set in Ingress loadBalancer.ingress"
        exit 1
    fi
fi

set +e
N=0
timeout=120
while true; do
    cat < /dev/null > /dev/tcp/${IP}/${PORT}
    if [[ $? -eq 0 ]]; then
        break
    fi
    if [ $N -ge ${timeout} ]; then
        echo "Timed out waiting for listener at ${IP}:${PORT}"
        exit 1
    fi
    sleep 1
    N=$(( N + 1 ))
done
set -e

varnishtest ${TESTOPTS} -Dport=${PORT} -Dip=${IP} cafe.vtc
