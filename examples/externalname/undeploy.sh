#! /bin/bash -ex

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../test/utils.sh

kubectl delete -f backend-cfg.yaml

kubectl delete -f cafe-ingress.yaml

kubectl delete -f ext-svcs.yaml

kubectl delete -f cafe.yaml

echo "Waiting until varnish-ingress Pods are not configured for Ingress"
wait_until_not_configured app=varnish-ingress
