#! /bin/bash -x

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../../test/utils.sh

LOCALPORT=${LOCALPORT:-4443}

wait_until_ready app=varnish-ingress

# Long timeout to wait for the Secret to appear as a certificate on
# the Pods.
wait_until_configured app=varnish-ingress default 600

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:443 >/dev/null &
ret=$?
if [ $ret -ne 0 ]; then
    exit $ret
fi
trap 'kill $(jobs -p)' EXIT

wait_for_port ${LOCALPORT}
set +e

sleep 1
# The test may be skipped (exit status 77) if haproxy is not installed.
varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
ret=$?
if [ $ret -eq 77 ]; then
    exit 0
elif [ $ret -ne 0 ]; then
    exit $ret
fi

exit 0

