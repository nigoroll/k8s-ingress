#! /bin/bash -x

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../test/utils.sh

LOCALPORT=${LOCALPORT:-8888}

set -e
wait_until_ready example=env
# Config check is at non-default port 9000.
wait_until_configured example=env default 120 9000

kubectl port-forward svc/pod-template-examples ${LOCALPORT}:81 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe_proxy.vtc
