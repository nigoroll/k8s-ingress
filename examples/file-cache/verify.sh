#! /bin/bash -x

MYDIR=$(dirname ${BASH_SOURCE[0]})
source ${MYDIR}/../../test/utils.sh

LOCALPORT=${LOCALPORT:-8888}

set -e
# Since Pods in StatefulSet come up sequentially, we need to
# explicitly wait for each of them to become ready. Otherwise the
# labels in subsequent waits might match only the first Pod, and
# port-forward may start too soon (before the second Pod is ready).
sleep 1
kubectl wait pod/viking-service-file-cache-0 --for=condition=Ready --timeout=2m
sleep 1
kubectl wait pod/viking-service-file-cache-1 --for=condition=Ready --timeout=2m

wait_until_configured example=file-cache

kubectl port-forward svc/viking-service-file-cache ${LOCALPORT}:80 >/dev/null &
trap 'kill $(jobs -p)' EXIT
wait_for_port ${LOCALPORT}

varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
