/*
 * Copyright (c) 2021 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	vcr_v1alpha1 "code.uplex.de/uplex-varnish/k8s-ingress/pkg/apis/varnishingress/v1alpha1"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish/vcl"
)

const ntmpls = 6

func (worker *NamespaceWorker) syncTcfg(key string) update.Status {
	worker.log.Infof("Syncing TemplateConfig: %s/%s", worker.namespace, key)
	tcfg, err := worker.tcfg.Get(key)
	if err != nil {
		return IncompleteIfNotFound(err, "%v", err)
	}
	worker.log.Tracef("TemplateConfig %s/%s: %+v", tcfg.Namespace,
		tcfg.Name, tcfg)

	updates := make([]string, 0, ntmpls)
	if tcfg.Spec.Ingress != "" {
		if err = vcl.SetIngressTmpl(tcfg.Spec.Ingress); err != nil {
			return update.MakeFatal(
				"TemplateConfig %s/%s: Cannot parse ingress "+
					"template: %v", tcfg.Namespace,
				tcfg.Name, err)
		}
		worker.log.Infof(
			"TemplateConfig %s/%s: updated ingress template",
			tcfg.Namespace, tcfg.Name)
		updates = append(updates, "ingress")
	}
	if tcfg.Spec.ACL != "" {
		if err = vcl.SetACLTmpl(tcfg.Spec.ACL); err != nil {
			return update.MakeFatal(
				"TemplateConfig %s/%s: Cannot parse ACL "+
					"template: %v", tcfg.Namespace,
				tcfg.Name, err)
		}
		worker.log.Infof("TemplateConfig %s/%s: updated ACL template",
			tcfg.Namespace, tcfg.Name)
		updates = append(updates, "acl")
	}
	if tcfg.Spec.Auth != "" {
		if err = vcl.SetAuthTmpl(tcfg.Spec.Auth); err != nil {
			return update.MakeFatal(
				"TemplateConfig %s/%s: Cannot parse auth "+
					"template: %v", tcfg.Namespace,
				tcfg.Name, err)
		}
		worker.log.Infof("TemplateConfig %s/%s: updated auth template",
			tcfg.Namespace, tcfg.Name)
		updates = append(updates, "auth")
	}
	if tcfg.Spec.ReqDisp != "" {
		if err = vcl.SetReqDispTmpl(tcfg.Spec.ReqDisp); err != nil {
			return update.MakeFatal(
				"TemplateConfig %s/%s: Cannot parse reqDisp "+
					"template: %v", tcfg.Namespace,
				tcfg.Name, err)
		}
		worker.log.Infof(
			"TemplateConfig %s/%s: updated reqDisp template",
			tcfg.Namespace, tcfg.Name)
		updates = append(updates, "reqDisp")
	}
	if tcfg.Spec.Rewrite != "" {
		if err = vcl.SetRewriteTmpl(tcfg.Spec.Rewrite); err != nil {
			return update.MakeFatal(
				"TemplateConfig %s/%s: Cannot parse rewrite "+
					"template: %v", tcfg.Namespace,
				tcfg.Name, err)
		}
		worker.log.Infof(
			"TemplateConfig %s/%s: updated rewrite template",
			tcfg.Namespace, tcfg.Name)
		updates = append(updates, "rewrite")
	}
	if tcfg.Spec.Shard != "" {
		if err = vcl.SetShardTmpl(tcfg.Spec.Shard); err != nil {
			return update.MakeFatal(
				"TemplateConfig %s/%s: Cannot parse shard "+
					"template: %v", tcfg.Namespace,
				tcfg.Name, err)
		}
		worker.log.Infof(
			"TemplateConfig %s/%s: updated shard template",
			tcfg.Namespace, tcfg.Name)
		updates = append(updates, "shard")
	}

	return update.MakeSuccess(
		"TemplateConfig %s/%s: updated templates: %v", tcfg.Namespace,
		tcfg.Name, updates)
}

func (worker *NamespaceWorker) addTcfg(key string) update.Status {
	return worker.syncTcfg(key)
}

func (worker *NamespaceWorker) updateTcfg(key string) update.Status {
	return worker.syncTcfg(key)
}

func (worker *NamespaceWorker) deleteTcfg(obj interface{}) update.Status {
	tcfg, ok := obj.(*vcr_v1alpha1.TemplateConfig)
	if !ok || tcfg == nil {
		worker.log.Warnf("Delete TemplateConfig: not found: %v", obj)
		vcl.ResetIngressTmpl()
		vcl.ResetACLTmpl()
		vcl.ResetAuthTmpl()
		vcl.ResetReqDispTmpl()
		vcl.ResetRewriteTmpl()
		vcl.ResetShardTmpl()
		return update.MakeSuccess("TemplateConfig: all templates reset")
	}

	worker.log.Infof("Deleting TemplateConfig: %s/%s", tcfg.Namespace,
		tcfg.Name)
	resets := make([]string, 0, ntmpls)

	if tcfg.Spec.Ingress != "" {
		vcl.ResetIngressTmpl()
		worker.log.Infof("TemplateConfig %s/%s: reset ingress template",
			tcfg.Namespace, tcfg.Name)
		resets = append(resets, "ingress")
	}
	if tcfg.Spec.ACL != "" {
		vcl.ResetACLTmpl()
		worker.log.Infof("TemplateConfig %s/%s: reset ACL template",
			tcfg.Namespace, tcfg.Name)
		resets = append(resets, "acl")
	}
	if tcfg.Spec.Auth != "" {
		vcl.ResetAuthTmpl()
		worker.log.Infof("TemplateConfig %s/%s: reset auth template",
			tcfg.Namespace, tcfg.Name)
		resets = append(resets, "auth")
	}
	if tcfg.Spec.ReqDisp != "" {
		vcl.ResetReqDispTmpl()
		worker.log.Infof("TemplateConfig %s/%s: reset reqDisp template",
			tcfg.Namespace, tcfg.Name)
		resets = append(resets, "reqDisp")
	}
	if tcfg.Spec.Rewrite != "" {
		vcl.ResetRewriteTmpl()
		worker.log.Infof("TemplateConfig %s/%s: reset rewrite template",
			tcfg.Namespace, tcfg.Name)
		resets = append(resets, "rewrite")
	}
	if tcfg.Spec.Shard != "" {
		vcl.ResetShardTmpl()
		worker.log.Infof("TemplateConfig %s/%s: reset shard template",
			tcfg.Namespace, tcfg.Name)
		resets = append(resets, "shard")
	}

	return update.MakeSuccess(
		"TemplateConfig %s/%s: reset templates: %v", tcfg.Namespace,
		tcfg.Name, resets)
}
