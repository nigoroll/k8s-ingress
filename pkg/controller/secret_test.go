/*
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"context"
	"io/ioutil"
	"testing"

	api_v1 "k8s.io/api/core/v1"
	net_v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes/fake"
	net_v1_listers "k8s.io/client-go/listers/networking/v1"
	"k8s.io/client-go/tools/cache"

	"github.com/sirupsen/logrus"
)

func setupIngLister(
	client *fake.Clientset,
	ns string,
) (net_v1_listers.IngressNamespaceLister, net_v1_listers.IngressClassLister) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	infFactory := informers.NewSharedInformerFactory(client, 0)
	ingInformer := infFactory.Networking().V1().Ingresses().Informer()
	ingclInformer := infFactory.Networking().V1().IngressClasses().
		Informer()
	ingLister := infFactory.Networking().V1().Ingresses().Lister()
	ingclLister := infFactory.Networking().V1().IngressClasses().Lister()
	ingNsLister := ingLister.Ingresses(ns)

	infFactory.Start(ctx.Done())
	cache.WaitForCacheSync(ctx.Done(),
		ingInformer.HasSynced, ingclInformer.HasSynced)
	return ingNsLister, ingclLister
}

func TestIngsForTLSSecret(t *testing.T) {
	ns := "test-ns"
	ingClass := "viking"
	ingName := "viking-tls-ingress"
	secretName := "viking-tls-secret"
	wrongIngClassSecret := "wrong-ing-class-secret"
	noIngClassSecret := "no-ing-class-secret"

	client := fake.NewSimpleClientset(
		&net_v1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      ingName,
				Annotations: map[string]string{
					ingressClassKey: ingClass,
				},
			},
			Spec: net_v1.IngressSpec{
				TLS: []net_v1.IngressTLS{{
					SecretName: secretName,
				}},
			},
		},
		&net_v1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      "wrong-ing-class",
				Annotations: map[string]string{
					ingressClassKey: "wrong-ing-class",
				},
			},
			Spec: net_v1.IngressSpec{
				TLS: []net_v1.IngressTLS{{
					SecretName: wrongIngClassSecret,
				}},
			},
		},
		&net_v1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      "no-ing-class",
			},
			Spec: net_v1.IngressSpec{
				TLS: []net_v1.IngressTLS{{
					SecretName: noIngClassSecret,
				}},
			},
		},
		&net_v1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      "different-secret-name",
				Annotations: map[string]string{
					ingressClassKey: ingClass,
				},
			},
			Spec: net_v1.IngressSpec{
				TLS: []net_v1.IngressTLS{{
					SecretName: "different-secret-name",
				}},
			},
		},
	)

	ingNsLister, ingclLister := setupIngLister(client, ns)
	worker := &NamespaceWorker{
		log:      &logrus.Logger{Out: ioutil.Discard},
		ing:      ingNsLister,
		ingClass: ingClass,
	}
	worker.listers = &Listers{
		ingcl: ingclLister,
	}

	secret := &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      secretName,
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, status := worker.getIngsForTLSSecret(secret)
	if status.IsError() {
		t.Fatal("getIngsForTLSSecret(): ", status)
	}
	if len(ings) != 1 {
		t.Errorf("getIngsForTLSSecret(): wanted 1 Ingress, got %d",
			len(ings))
	}
	if ings[0].Name != ingName {
		t.Errorf("getIngsForTLSSecret(): Ingress name wanted %s, "+
			"got %s", ingName, ings[0].Name)
	}
	if is, status := worker.
		isVikingIngressTLSSecret(secret); status.IsError() {
		t.Fatal("isVikingIngressTLSSecret(): ", status)
	} else if !is {
		t.Error("isVikingIngressTLSSecret(): wanted true, got false")
	}

	secret = &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      "no-ing-for-secret",
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, status = worker.getIngsForTLSSecret(secret)
	if status.IsError() {
		t.Fatal("getIngsForTLSSecret(): ", status)
	}
	if ings != nil || len(ings) != 0 {
		t.Error("getIngsForTLSSecret(): wanted no Ingresses, got >0")
	}
	if is, status := worker.
		isVikingIngressTLSSecret(secret); status.IsError() {
		t.Fatal("isVikingIngressTLSSecret(): ", status)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}

	secret = &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      wrongIngClassSecret,
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, status = worker.getIngsForTLSSecret(secret)
	if status.IsError() {
		t.Fatal("getIngsForTLSSecret(): ", status)
	}
	if ings != nil || len(ings) != 0 {
		t.Error("getIngsForTLSSecret(): wanted no Ingresses, got >0")
	}
	if is, status := worker.
		isVikingIngressTLSSecret(secret); status.IsError() {
		t.Fatal("isVikingIngressTLSSecret(): ", status)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}

	secret = &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      noIngClassSecret,
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, status = worker.getIngsForTLSSecret(secret)
	if status.IsError() {
		t.Fatal("getIngsForTLSSecret(): ", status)
	}
	if ings != nil || len(ings) != 0 {
		t.Error("getIngsForTLSSecret(): wanted no Ingresses, got >0")
	}
	if is, status := worker.
		isVikingIngressTLSSecret(secret); status.IsError() {
		t.Fatal("isVikingIngressTLSSecret(): ", status)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}
}

func TestNoIngsForTLSSecret(t *testing.T) {
	ns := "test-ns"
	ingClass := "viking"

	client := fake.NewSimpleClientset()
	ingNsLister, ingclLister := setupIngLister(client, ns)
	worker := &NamespaceWorker{
		log:      &logrus.Logger{Out: ioutil.Discard},
		ing:      ingNsLister,
		ingClass: ingClass,
	}
	worker.listers = &Listers{
		ingcl: ingclLister,
	}
	secret := &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      "test-secret",
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, status := worker.getIngsForTLSSecret(secret)
	if status.IsError() {
		t.Fatal("getIngsForTLSSecret(): ", status)
	}
	if len(ings) != 0 {
		t.Errorf("getIngsForTLSSecret(): wanted 0 Ingresses, got %d",
			len(ings))
	}
	if is, status := worker.
		isVikingIngressTLSSecret(secret); status.IsError() {
		t.Fatal("isVikingIngressTLSSecret(): ", status)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}
}
