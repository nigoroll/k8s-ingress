/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"fmt"
	"sync"
	"time"

	api_v1 "k8s.io/api/core/v1"
	net_v1 "k8s.io/api/networking/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	core_v1_listers "k8s.io/client-go/listers/core/v1"
	net_v1_listers "k8s.io/client-go/listers/networking/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"

	"github.com/sirupsen/logrus"

	ving_v1alpha1 "code.uplex.de/uplex-varnish/k8s-ingress/pkg/apis/varnishingress/v1alpha1"
	vcr_listers "code.uplex.de/uplex-varnish/k8s-ingress/pkg/client/listers/varnishingress/v1alpha1"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish"
)

// NamespaceWorker serves fanout of work items to workers for each
// namespace for which the controller is notified about a resource
// update. The NamespaceQueues object creates a new instance when it
// reads an item from a new namespace from its main queue. Each worker
// has its own queue and listers filtered for its namespace. Thus each
// namespace is synced separately and sequentially, and all of the
// namespaces are synced in parallel.
type NamespaceWorker struct {
	namespace         string
	ingClass          string
	varnishImpl       string
	log               *logrus.Logger
	vController       *varnish.Controller
	hController       *haproxy.Controller
	queue             workqueue.RateLimitingInterface
	listers           *Listers
	ing               net_v1_listers.IngressNamespaceLister
	svc               core_v1_listers.ServiceNamespaceLister
	endp              core_v1_listers.EndpointsNamespaceLister
	tsecr             core_v1_listers.SecretNamespaceLister
	vsecr             core_v1_listers.SecretNamespaceLister
	vcfg              vcr_listers.VarnishConfigNamespaceLister
	bcfg              vcr_listers.BackendConfigNamespaceLister
	tcfg              vcr_listers.TemplateConfigNamespaceLister
	client            kubernetes.Interface
	recorder          record.EventRecorder
	wg                *sync.WaitGroup
	incomplRetryDelay time.Duration
}

// Note that there is no NamespaceLister for IngressClass, which is not
// namespaced. We can only use listers.ingcl

func (worker *NamespaceWorker) event(obj interface{}, evtType, reason,
	msgFmt string, args ...interface{}) {

	eventObj := obj
	if syncObj, ok := obj.(*SyncObj); ok {
		eventObj = syncObj.Obj
	}
	deleted, ok := eventObj.(cache.DeletedFinalStateUnknown)
	if ok {
		worker.log.Warnf("Event %s %s: final state unknown: %s",
			evtType, reason, deleted.Key)
		eventObj = deleted.Obj
	}
	kind := "Unknown"
	switch eventObj.(type) {
	case *net_v1.Ingress:
		ing, _ := eventObj.(*net_v1.Ingress)
		worker.recorder.Eventf(ing, evtType, reason, msgFmt, args...)
		kind = "Ingress"
	case *net_v1.IngressClass:
		ingcl, _ := eventObj.(*net_v1.IngressClass)
		worker.recorder.Eventf(ingcl, evtType, reason, msgFmt, args...)
		kind = "IngressClass"
	case *api_v1.Service:
		svc, _ := eventObj.(*api_v1.Service)
		worker.recorder.Eventf(svc, evtType, reason, msgFmt, args...)
		kind = "Service"
	case *api_v1.Endpoints:
		endp, _ := eventObj.(*api_v1.Endpoints)
		worker.recorder.Eventf(endp, evtType, reason, msgFmt, args...)
		kind = "Endpoints"
	case *api_v1.Secret:
		secr, _ := eventObj.(*api_v1.Secret)
		worker.recorder.Eventf(secr, evtType, reason, msgFmt, args...)
		kind = "Secret"
	case *ving_v1alpha1.VarnishConfig:
		vcfg, _ := eventObj.(*ving_v1alpha1.VarnishConfig)
		worker.recorder.Eventf(vcfg, evtType, reason, msgFmt, args...)
		kind = "VarnishConfig"
	case *ving_v1alpha1.BackendConfig:
		bcfg, _ := eventObj.(*ving_v1alpha1.BackendConfig)
		worker.recorder.Eventf(bcfg, evtType, reason, msgFmt, args...)
		kind = "BackendConfig"
	case *ving_v1alpha1.TemplateConfig:
		bcfg, _ := eventObj.(*ving_v1alpha1.TemplateConfig)
		worker.recorder.Eventf(bcfg, evtType, reason, msgFmt, args...)
		kind = "TemplateConfig"
	default:
		worker.log.Warnf("Unhandled type %T, no event generated",
			eventObj)
	}
	syncCounters.WithLabelValues(worker.namespace, kind, reason).Inc()
}

func (worker *NamespaceWorker) infoEvent(obj interface{}, reason, msgFmt string,
	args ...interface{}) {

	worker.event(obj, api_v1.EventTypeNormal, reason, msgFmt, args...)
}

func (worker *NamespaceWorker) warnEvent(obj interface{}, reason, msgFmt string,
	args ...interface{}) {

	worker.event(obj, api_v1.EventTypeWarning, reason, msgFmt, args...)
}

func (worker *NamespaceWorker) syncSuccess(obj interface{}, msgPfx string,
	status update.Status) {

	worker.log.Infof("%s%s", msgPfx, status)
	worker.infoEvent(obj, status.Reason(), "%s%s", msgPfx, status)
}

func (worker *NamespaceWorker) syncFailure(obj interface{}, msgPfx string,
	status update.Status) {

	worker.log.Errorf("%s%s", msgPfx, status)
	worker.warnEvent(obj, status.Reason(), "%s%s", msgPfx, status)
}

func (worker *NamespaceWorker) dispatch(obj interface{}) update.Status {
	syncObj, ok := obj.(*SyncObj)
	if !ok {
		return update.MakeFatal("Unhandled type %T", obj)
	}
	_, key, err := getNameSpace(syncObj.Obj)
	if err != nil {
		return update.MakeFatal("Cannot get key for object %v: %v",
			syncObj.Obj, err)
	}
	switch syncObj.Type {
	case Add:
		switch syncObj.Obj.(type) {
		case *net_v1.Ingress:
			return worker.addIng(key)
		case *net_v1.IngressClass:
			return worker.addIngClass(key)
		case *api_v1.Service:
			return worker.addSvc(key)
		case *api_v1.Endpoints:
			return worker.addEndp(key)
		case *api_v1.Secret:
			return worker.addSecret(key)
		case *ving_v1alpha1.VarnishConfig:
			return worker.addVcfg(key)
		case *ving_v1alpha1.BackendConfig:
			return worker.addBcfg(key)
		case *ving_v1alpha1.TemplateConfig:
			return worker.addTcfg(key)
		default:
			return update.MakeFatal("Unhandled object type: %T",
				syncObj.Obj)
		}
	case Update:
		switch syncObj.Obj.(type) {
		case *net_v1.Ingress:
			return worker.updateIng(key)
		case *net_v1.IngressClass:
			return worker.updateIngClass(key)
		case *api_v1.Service:
			return worker.updateSvc(key)
		case *api_v1.Endpoints:
			return worker.updateEndp(key)
		case *api_v1.Secret:
			return worker.updateSecret(key)
		case *ving_v1alpha1.VarnishConfig:
			return worker.updateVcfg(key)
		case *ving_v1alpha1.BackendConfig:
			return worker.updateBcfg(key)
		case *ving_v1alpha1.TemplateConfig:
			return worker.updateTcfg(key)
		default:
			return update.MakeFatal("Unhandled object type: %T",
				syncObj.Obj)
		}
	case Delete:
		deletedObj := syncObj.Obj
		deleted, ok := deletedObj.(cache.DeletedFinalStateUnknown)
		if ok {
			worker.log.Warnf("delete %s: final state unknown: %s",
				key, deleted.Key)
			deletedObj = deleted.Obj
		}
		switch deletedObj.(type) {
		case *net_v1.Ingress:
			return worker.deleteIng(deletedObj)
		case *net_v1.IngressClass:
			return worker.deleteIngClass(deletedObj)
		case *api_v1.Service:
			return worker.deleteSvc(deletedObj)
		case *api_v1.Endpoints:
			return worker.deleteEndp(deletedObj)
		case *api_v1.Secret:
			return worker.deleteSecret(deletedObj)
		case *ving_v1alpha1.VarnishConfig:
			return worker.deleteVcfg(deletedObj)
		case *ving_v1alpha1.BackendConfig:
			return worker.deleteBcfg(deletedObj)
		case *ving_v1alpha1.TemplateConfig:
			return worker.deleteTcfg(deletedObj)
		default:
			return update.MakeFatal("Unhandled object type: %T",
				syncObj.Obj)
		}
	default:
		return update.MakeFatal("Unhandled sync type: %v", syncObj.Type)
	}
}

func (worker *NamespaceWorker) next() {
	obj, quit := worker.queue.Get()
	if quit {
		return
	}
	defer worker.queue.Done(obj)

	status := worker.dispatch(obj)
	msgPfx := ""
	if syncObj, ok := obj.(*SyncObj); ok {
		if ns, name, err := getNameSpace(syncObj.Obj); err == nil {
			msgPfx = syncObj.Type.String() + " " + ns + "/" +
				name + ": "
		}
	}

	switch status.Type {
	case update.Noop:
		// XXX configure a verbose mode that also generates
		// Events for the Noop case
		worker.log.Info(msgPfx, status.String())
		worker.queue.Forget(obj)
	case update.Success:
		worker.syncSuccess(obj, msgPfx, status)
		worker.queue.Forget(obj)
	case update.Fatal:
		worker.syncFailure(obj, msgPfx, status)
		worker.queue.Forget(obj)
	case update.Recoverable:
		worker.syncFailure(obj, msgPfx, status)
		worker.queue.AddRateLimited(obj)
	case update.Incomplete:
		worker.syncFailure(obj, msgPfx, status)
		worker.queue.AddAfter(obj, worker.incomplRetryDelay)
	}
}

func (worker *NamespaceWorker) work() {
	defer worker.wg.Done()

	worker.log.Info("Starting worker for namespace:", worker.namespace)

	for !worker.queue.ShuttingDown() {
		worker.next()
	}

	worker.log.Info("Shutting down worker for namespace:", worker.namespace)
}

// NamespaceQueues reads from the main queue to which informers add
// new work items from all namespaces. The worker reads items from the
// queue and places them on separate queues for NamespaceWorkers
// responsible for each namespace.
type NamespaceQueues struct {
	Queue             workqueue.RateLimitingInterface
	ingClass          string
	varnishImpl       string
	log               *logrus.Logger
	vController       *varnish.Controller
	hController       *haproxy.Controller
	workers           map[string]*NamespaceWorker
	listers           *Listers
	client            kubernetes.Interface
	recorder          record.EventRecorder
	wg                *sync.WaitGroup
	incomplRetryDelay time.Duration
	devMode           bool
}

// NewNamespaceQueues creates a NamespaceQueues object.
//
//	log: logger initialized at startup
//	ingClass: value of the ingress.class Ingress annotation
//	vController: Varnish controller initialied at startup
//	listers: client-go/lister instance for each resource type
//	client: k8s API client initialized at startup
//	recorder: Event broadcaster initialized at startup
func NewNamespaceQueues(
	log *logrus.Logger,
	ingClass string,
	vController *varnish.Controller,
	hController *haproxy.Controller,
	listers *Listers,
	client kubernetes.Interface,
	recorder record.EventRecorder,
	incomplRetryDelay time.Duration,
	devMode bool,
	varnishImpl string,
) *NamespaceQueues {

	q := workqueue.NewNamedRateLimitingQueue(
		workqueue.DefaultControllerRateLimiter(), "_ALL_")
	return &NamespaceQueues{
		Queue:             q,
		log:               log,
		ingClass:          ingClass,
		varnishImpl:       varnishImpl,
		vController:       vController,
		hController:       hController,
		workers:           make(map[string]*NamespaceWorker),
		listers:           listers,
		client:            client,
		recorder:          recorder,
		wg:                new(sync.WaitGroup),
		incomplRetryDelay: incomplRetryDelay,
		devMode:           devMode,
	}
}

func getNameSpace(obj interface{}) (namespace, name string, err error) {
	k, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
	if err != nil {
		return
	}
	namespace, name, err = cache.SplitMetaNamespaceKey(k)
	return
}

func (qs *NamespaceQueues) next() {
	obj, quit := qs.Queue.Get()
	if quit {
		return
	}
	defer qs.Queue.Done(obj)

	syncObj, ok := obj.(*SyncObj)
	if !ok {
		utilruntime.HandleError(fmt.Errorf("Unhandled type %T", obj))
		return
	}
	ns, _, err := getNameSpace(syncObj.Obj)
	if err != nil {
		utilruntime.HandleError(err)
		return
	}
	worker, exists := qs.workers[ns]
	if !exists {
		q := workqueue.NewNamedRateLimitingQueue(
			workqueue.DefaultControllerRateLimiter(), ns)
		worker = &NamespaceWorker{
			namespace:         ns,
			ingClass:          qs.ingClass,
			varnishImpl:       qs.varnishImpl,
			log:               qs.log,
			vController:       qs.vController,
			hController:       qs.hController,
			queue:             q,
			listers:           qs.listers,
			ing:               qs.listers.ing.Ingresses(ns),
			svc:               qs.listers.svc.Services(ns),
			endp:              qs.listers.endp.Endpoints(ns),
			vsecr:             qs.listers.vsecr.Secrets(ns),
			tsecr:             qs.listers.tsecr.Secrets(ns),
			vcfg:              qs.listers.vcfg.VarnishConfigs(ns),
			bcfg:              qs.listers.bcfg.BackendConfigs(ns),
			client:            qs.client,
			recorder:          qs.recorder,
			wg:                qs.wg,
			incomplRetryDelay: qs.incomplRetryDelay,
		}
		if qs.devMode {
			worker.tcfg = qs.listers.tcfg.TemplateConfigs(ns)
		}
		qs.workers[ns] = worker
		qs.wg.Add(1)
		go worker.work()
	}
	worker.queue.Add(obj)
	qs.Queue.Forget(obj)
}

// StopNS stops the queue for a namespace and its associated
// worker. The NamespaceWorker object is de-referenced, becoming
// available for garbage collection.
func (qs *NamespaceQueues) StopNS(ns string) {
	worker, exists := qs.workers[ns]
	if !exists {
		return
	}
	qs.log.Infof("Shutting down queue for namespace: %s", worker.namespace)
	worker.queue.ShutDown()
	// XXX sync the map
	delete(qs.workers, ns)
}

// Run comprises the main loop of the controller, reading from the
// main queue of work items and handing them off to workers for each
// namespace.
func (qs *NamespaceQueues) Run() {
	qs.log.Info("Starting dispatcher worker")
	for !qs.Queue.ShuttingDown() {
		qs.next()
	}
}

// Stop shuts down the main queue loop initiated by Run(), and in turn
// shuts down all of the NamespaceWorkers.
func (qs *NamespaceQueues) Stop() {
	qs.log.Info("Shutting down dispatcher worker")
	qs.Queue.ShutDown()
	for _, worker := range qs.workers {
		qs.log.Infof("Shutting down queue for namespace: %s",
			worker.namespace)
		worker.queue.ShutDown()
	}
	qs.log.Info("Waiting for workers to shut down")
	qs.wg.Wait()
}
