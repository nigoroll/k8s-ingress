/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"reflect"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish/vcl"

	api_v1 "k8s.io/api/core/v1"
	net_v1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

// XXX make this configurable
const (
	admPortName      = "varnishadm"
	tlsPortName      = "tls"
	dplanePortName   = "dataplane"
	crtDnldrPortName = "crt-dnldr"
)

// isVarnishIngSvc determines if a Service represents a Varnish that
// can implement Ingress, for which this controller is responsible.
// Currently the app label must point to a hardwired name, and a
// hardwired admin port name must be defined for one of the Endpoints.
func (worker *NamespaceWorker) isVarnishIngSvc(svc *api_v1.Service) bool {
	app, exists := svc.Labels[labelKey]
	if !exists || app != labelVal {
		return false
	}
	for _, port := range svc.Spec.Ports {
		worker.log.Debugf("Service %s/%s port: %+v", svc.Namespace,
			svc.Name, port)
		if port.Name == admPortName {
			return true
		}
	}
	return false
}

func (worker *NamespaceWorker) getIngsForSvc(
	svc *api_v1.Service,
) (ings []*net_v1.Ingress, status update.Status) {
	allIngs, err := worker.ing.List(labels.Everything())
	if err != nil {
		if errors.IsNotFound(err) {
			status = update.MakeNoop(
				"No Ingress defined in namespace %s",
				svc.Namespace)
			syncCounters.WithLabelValues(worker.namespace,
				"Service", "Ignore").Inc()
		} else {
			status = update.MakeRecoverable("%v", err)
		}
		return
	}

	for _, ing := range allIngs {
		if ing.Namespace != svc.Namespace {
			// Shouldn't be possible
			continue
		}
		cpy := ing.DeepCopy()
		if cpy.Spec.DefaultBackend != nil {
			if cpy.Spec.DefaultBackend.Service.Name == svc.Name {
				ings = append(ings, cpy)
			}
		}
		for _, rules := range cpy.Spec.Rules {
			if rules.IngressRuleValue.HTTP == nil {
				continue
			}
			for _, p := range rules.IngressRuleValue.HTTP.Paths {
				if p.Backend.Service.Name == svc.Name {
					ings = append(ings, cpy)
				}
			}
		}
	}

	if len(ings) == 0 {
		status = update.MakeNoop(
			"No Varnish Ingresses defined for service %s/%s",
			svc.Namespace, svc.Name)
		syncCounters.WithLabelValues(worker.namespace, "Service",
			"Ignore").Inc()
		return
	}
	status = update.MakeSuccess("")
	return
}

func (worker *NamespaceWorker) enqueueIngressForService(
	svc *api_v1.Service) update.Status {

	ings, status := worker.getIngsForSvc(svc)
	if status.Type != update.Success {
		return status
	}
	requeued := make([]string, 0, len(ings))
	for _, ing := range ings {
		if isViking, err := worker.isVikingIngress(ing); err != nil {
			return update.MakeRecoverable(
				"Error checking if Ingress %s/%s is to be "+
					"implemented by viking: %v",
				ing.Namespace, ing.Name, err)
		} else if !isViking {
			continue
		}
		worker.queue.Add(&SyncObj{Type: Update, Obj: ing})
		requeued = append(requeued, ing.Namespace+"/"+ing.Name)
	}
	if len(requeued) == 0 {
		return update.MakeNoop(
			"Service %s/%s: not an IngressBackend for any "+
				"viking Ingress", svc.Namespace, svc.Name)
	}
	return update.MakeSuccess(
		"Service %s/%s: re-syncing Ingresses for IngressBackend: %s",
		svc.Namespace, svc.Name, requeued)
}

// Return true if changes in Varnish services may lead to changes in
// the VCL config generated for the Ingress.
func (worker *NamespaceWorker) isVarnishInVCLSpec(ing *net_v1.Ingress) bool {
	vcfgs, err := worker.vcfg.List(labels.Everything())
	if err != nil {
		worker.log.Warnf("Error retrieving VarnishConfigs in "+
			"namespace %s: %v", worker.namespace, err)
		return false
	}
	for _, vcfg := range vcfgs {
		if vcfg.Spec.SelfSharding != nil {
			return true
		}
	}
	return false
}

func epAddrs2VCLAddrs(
	epAddrs []api_v1.EndpointAddress,
	vclAddrs []vcl.Address,
	admPort int32,
) []vcl.Address {
	for _, epAddr := range epAddrs {
		vclAddr := vcl.Address{
			IP:   epAddr.IP,
			Port: admPort,
		}
		vclAddr.PodNamespace, vclAddr.PodName = getTargetPod(epAddr)
		vclAddrs = append(vclAddrs, vclAddr)
	}
	return vclAddrs
}

func epAddrs2OffldAddrs(
	epAddrs []api_v1.EndpointAddress,
	offldAddrs []haproxy.OffldAddr,
	dplanePort, crtDnldrPort int32,
) []haproxy.OffldAddr {
	for _, epAddr := range epAddrs {
		offldAddr := haproxy.OffldAddr{
			IP:            epAddr.IP,
			DataplanePort: dplanePort,
			CrtDnldrPort:  crtDnldrPort,
		}
		offldAddr.PodNamespace, offldAddr.PodName = getTargetPod(epAddr)
		offldAddrs = append(offldAddrs, offldAddr)
	}
	return offldAddrs
}

func (worker *NamespaceWorker) svc2Addrs(
	svc *api_v1.Service,
) (vaddrs []vcl.Address, offldAddrs []haproxy.OffldAddr, status update.Status) {
	endps, err := worker.getServiceEndpoints(svc)
	if err != nil {
		status = IncompleteIfNotFound(err, "%v", err)
		return
	}
	worker.log.Tracef("Varnish service %s/%s endpoints: %+v", svc.Namespace,
		svc.Name, endps)
	if endps == nil {
		status = update.MakeIncomplete(
			"could not find endpoints for service: %s/%s",
			svc.Namespace, svc.Name)
		return
	}

	// XXX hard-wired Port names
	for _, subset := range endps.Subsets {
		admPort, dplanePort := int32(0), int32(0)
		crtDnldrPort := int32(0)
		hasTLS := false
		for _, port := range subset.Ports {
			switch port.Name {
			case admPortName:
				admPort = port.Port
			case dplanePortName:
				hasTLS = true
				dplanePort = port.Port
			case crtDnldrPortName:
				hasTLS = true
				crtDnldrPort = port.Port
			}
		}
		if admPort == 0 {
			status = update.MakeFatal(
				"No Varnish admin port %s found for Service "+
					"%s/%s endpoint",
				admPortName, svc.Namespace, svc.Name)
			return
		}
		vaddrs = epAddrs2VCLAddrs(subset.Addresses, vaddrs, admPort)
		vaddrs = epAddrs2VCLAddrs(subset.NotReadyAddresses, vaddrs,
			admPort)
		if hasTLS {
			offldAddrs = epAddrs2OffldAddrs(subset.Addresses,
				offldAddrs, dplanePort, crtDnldrPort)
			offldAddrs = epAddrs2OffldAddrs(
				subset.NotReadyAddresses, offldAddrs,
				dplanePort, crtDnldrPort)
		}
	}
	status = update.MakeSuccess("")
	return
}

func (worker *NamespaceWorker) syncSvc(key string) update.Status {
	worker.log.Infof("Syncing Service: %s/%s", worker.namespace, key)
	svc, err := worker.svc.Get(key)
	if err != nil {
		return IncompleteIfNotFound(err, "%v", err)
	}

	// If this Service is labelled as a public viking Service,
	// update status.loadBalancer for Ingresses that it
	// implements.
	if pubSvcVal, exists := svc.Labels[vikingPubSvcKey]; exists &&
		pubSvcVal == vikingPubSvcVal {
		admSvcs, err := worker.svc.List(varnishIngressSelector)
		if err != nil {
			if !errors.IsNotFound(err) {
				return update.MakeFatal(
					"Cannot list admin Services for "+
						"public viking Service "+
						"%s/%s: %v",
					svc.Namespace, svc.Name, err)
			}
		} else {
			for _, admSvc := range admSvcs {
				if !worker.isVarnishIngSvc(admSvc) {
					continue
				}
				if !reflect.DeepEqual(svc.Spec.Selector,
					admSvc.Spec.Selector) {
					continue
				}
				ings, err := worker.getIngsForVarnishSvc(admSvc)
				if err != nil && !errors.IsNotFound(err) {
					return update.MakeFatal(
						"Cannot list Ingresses for "+
							"viking admin Service "+
							"%s/%s: %v",
						admSvc.Namespace, admSvc.Name,
						err)
				}
				if ings != nil && len(ings) > 0 {
					status := worker.updateIngStatus(
						admSvc, ings)
					if status.IsError() {
						return status
					}
				}
			}
		}
	}

	if !worker.isVarnishIngSvc(svc) {
		return worker.enqueueIngressForService(svc)
	}

	worker.log.Infof("Syncing Varnish Ingress Service %s/%s:",
		svc.Namespace, svc.Name)
	if svc.Spec.Type == api_v1.ServiceTypeExternalName {
		return update.MakeFatal(
			"Viking Service %s/%s: type ExternalName is illegal",
			svc.Namespace, svc.Name)
	}

	// Check if there are Ingresses for which the VCL spec may
	// change due to changes in Varnish services.
	updateVCL := false
	ings, err := worker.ing.List(labels.Everything())
	if err != nil {
		return IncompleteIfNotFound(err, "%v", err)
	}
	for _, ing := range ings {
		if ing.Namespace != svc.Namespace {
			continue
		}
		ingSvc, err := worker.getVarnishSvcForIng(ing)
		if err != nil {
			return IncompleteIfNotFound(err, "%v", err)
		}
		if ingSvc == nil {
			continue
		}
		worker.log.Debugf("Ingress %s/%s: Service %s/%s", ing.Namespace,
			ing.Name, svc.Namespace, svc.Name)
		if ingSvc.Namespace != svc.Namespace ||
			ingSvc.Name != svc.Name {
			continue
		}
		worker.log.Debugf("Ingress %s/%s: %+v", ing.Namespace, ing.Name,
			ing)
		svcKey := svc.Namespace + "/" + svc.Name
		if worker.vController.HasVarnishSvc(svcKey) &&
			!worker.isVarnishInVCLSpec(ing) {
			continue
		}
		updateVCL = true
		worker.log.Debugf("Requeueing Ingress %s/%s after changed "+
			"Varnish service %s/%s: %+v", ing.Namespace,
			ing.Name, svc.Namespace, svc.Name, ing)
		worker.queue.Add(&SyncObj{Type: Update, Obj: ing})
	}
	if !updateVCL {
		worker.log.Debugf("No change in VCL due to changed Varnish "+
			"service %s/%s", svc.Namespace, svc.Name)
	}

	addrs, offldAddrs, status := worker.svc2Addrs(svc)
	if status.IsError() {
		return status
	}

	worker.log.Tracef("Searching annotations for the secret for %s/%s",
		svc.Namespace, svc.Name)
	secrName, ok := svc.Annotations[vikingAdmSecretKey]
	if !ok {
		return update.MakeFatal(
			"Service %s/%s: missing required annotation %s",
			svc.Namespace, svc.Name, vikingAdmSecretKey)
	}
	worker.log.Infof("Found secret name %s for Service %s/%s", secrName,
		svc.Namespace, svc.Name)

	if secret, err := worker.vsecr.Get(secrName); err == nil {
		err = worker.setSecret(secret)
		if err != nil {
			return update.MakeIncomplete("%v", err)
		}
	} else {
		worker.log.Warnf("Cannot get Secret %s: %v", secrName, err)
	}

	if len(offldAddrs) > 0 {
		worker.log.Tracef("Varnish service %s/%s offloader addresses: "+
			"%+v", svc.Namespace, svc.Name, offldAddrs)
		status := worker.hController.AddOrUpdateOffloader(
			svc.Namespace+"/"+svc.Name, offldAddrs,
			svc.Namespace+"/"+secrName)
		if status.IsError() {
			return status
		}
	}
	worker.log.Tracef("Varnish service %s/%s addresses: %+v", svc.Namespace,
		svc.Name, addrs)
	return worker.vController.AddOrUpdateVarnishSvc(
		svc.Namespace+"/"+svc.Name, addrs,
		svc.Namespace+"/"+secrName, !updateVCL)
}

func (worker *NamespaceWorker) addSvc(key string) update.Status {
	return worker.syncSvc(key)
}

func (worker *NamespaceWorker) updateSvc(key string) update.Status {
	return worker.syncSvc(key)
}

func (worker *NamespaceWorker) deleteSvc(obj interface{}) update.Status {
	svc, ok := obj.(*api_v1.Service)
	if !ok || svc == nil {
		return update.MakeNoop("Delete Service: not found: %v", obj)
	}
	nsKey := svc.Namespace + "/" + svc.Name
	worker.log.Info("Deleting Service: ", nsKey)

	deletedVarnish := false
	deletedOffldr := false
	if worker.vController.HasVarnishSvc(nsKey) {
		if status := worker.vController.
			DeleteVarnishSvc(nsKey); status.IsError() {
			return status
		}
		deletedVarnish = true
	}
	if worker.hController.HasOffloader(nsKey) {
		if status := worker.hController.
			DeleteOffldSvc(nsKey); status.IsError() {
			return status
		}
		deletedOffldr = true
	}

	if !worker.isVarnishIngSvc(svc) {
		return worker.enqueueIngressForService(svc)
	}

	if deletedVarnish || deletedOffldr {
		return update.MakeSuccess("")
	}
	return update.MakeNoop("Service %s/%s: neither a viking admin Service"+
		" nor an Ingress Backend", svc.Namespace, svc.Name)
}
