/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"fmt"
	"strings"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"

	vcr_v1alpha1 "code.uplex.de/uplex-varnish/k8s-ingress/pkg/apis/varnishingress/v1alpha1"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish/vcl"

	api_v1 "k8s.io/api/core/v1"
	net_v1 "k8s.io/api/networking/v1"
)

const cookieKeyPfx = "cookie="

// Don't return error (requeuing the vcfg) if either of Ingresses or
// Services are not found -- they will sync as needed when and if they
// are discovered.
func (worker *NamespaceWorker) enqueueIngsForVcfg(
	vcfg *vcr_v1alpha1.VarnishConfig) update.Status {

	svc2ing := make(map[*api_v1.Service]*net_v1.Ingress)
	ings, err := worker.ing.List(labels.Everything())
	if errors.IsNotFound(err) {
		return update.MakeNoop("VarnishConfig %s/%s: no Ingresses "+
			"found in workspace %s", vcfg.Namespace, vcfg.Name,
			worker.namespace)
	}
	if err != nil {
		return update.MakeRecoverable("%v", err)
	}
	for _, ing := range ings {
		if isViking, err := worker.isVikingIngress(ing); err != nil {
			return update.MakeRecoverable(
				"Error checking if Ingress %s/%s is to be "+
					"implemented by viking: %v",
				ing.Namespace, ing.Name, err)
		} else if !isViking {
			continue
		}
		vSvc, err := worker.getVarnishSvcForIng(ing)
		if errors.IsNotFound(err) {
			return update.MakeNoop("VarnishConfig %s/%s: no "+
				"Varnish Services found in workspace %s",
				vcfg.Namespace, vcfg.Name, worker.namespace)
		}
		if err != nil {
			return update.MakeRecoverable("%v", err)
		}
		if vSvc != nil {
			svc2ing[vSvc] = ing
		}
	}

	svcSet := make(map[string]struct{})
	for _, svc := range vcfg.Spec.Services {
		if _, exists := svcSet[svc]; exists {
			continue
		}
		svcSet[svc] = struct{}{}

		svcObj, err := worker.svc.Get(svc)
		if err != nil {
			return IncompleteIfNotFound(err, "%v", err)
		}
		if ing, exists := svc2ing[svcObj]; exists {
			worker.log.Infof("VarnishConfig %s/%s: enqueuing "+
				"Ingress %s/%s for update", vcfg.Namespace,
				vcfg.Name, ing.Namespace, ing.Name)
			worker.queue.Add(&SyncObj{Type: Update, Obj: ing})
		}
	}
	return update.MakeSuccess(
		"VarnishConfig %s/%s: re-queued Ingress(es)", vcfg.Namespace,
		vcfg.Name)
}

// XXX a validating webhook should do this
func validateRewrites(rewrites []vcr_v1alpha1.RewriteSpec) error {
	for _, rw := range rewrites {
		if rw.Method == vcr_v1alpha1.Delete &&
			strings.HasSuffix(rw.Target, ".url") {

			return fmt.Errorf("target %s may not be deleted",
				rw.Target)
		}
		if rw.Source != "" && (strings.HasPrefix(rw.Target, "be") !=
			strings.HasPrefix(rw.Source, "be")) {

			return fmt.Errorf("target %s and source %s illegally "+
				"mix client and backend contexts", rw.Target,
				rw.Source)
		}
		if rw.Compare != vcr_v1alpha1.Prefix &&
			(rw.Select == vcr_v1alpha1.Exact ||
				rw.Select == vcr_v1alpha1.Longest ||
				rw.Select == vcr_v1alpha1.Shortest) {

			return fmt.Errorf("select value %s not permitted with "+
				"compare value %s", rw.Select, rw.Compare)
		}
		if rw.Compare != vcr_v1alpha1.Match &&
			rw.MatchFlags != nil &&
			((rw.MatchFlags.MaxMem != nil &&
				*rw.MatchFlags.MaxMem != 0) ||
				(rw.MatchFlags.Anchor != "" &&
					rw.MatchFlags.Anchor != vcr_v1alpha1.None) ||
				rw.MatchFlags.UTF8 ||
				rw.MatchFlags.PosixSyntax ||
				rw.MatchFlags.LongestMatch ||
				rw.MatchFlags.Literal ||
				rw.MatchFlags.NeverCapture ||
				rw.MatchFlags.PerlClasses ||
				rw.MatchFlags.WordBoundary) {

			return fmt.Errorf("Only the case-sensitive match flag " +
				"may be set for fixed-string matches")
		}
		// The same Value may not be added in more than one Rule.
		// The Rewrite field is required, unless the method is Delete.
		vals := make(map[string]struct{})
		for _, rule := range rw.Rules {
			if _, exists := vals[rule.Value]; exists {
				return fmt.Errorf("Value \"%s\" appears in "+
					"more than one rule", rule.Value)
			}
			vals[rule.Value] = struct{}{}

			if rw.Method != vcr_v1alpha1.Delete &&
				rule.Rewrite == "" {

				return fmt.Errorf("Rewrite field may not be " +
					"empty, unless the method is delete")
			}
		}
		// XXX what else?
	}
	return nil
}

// XXX validating webhook should do this
func validateReqDisps(reqDisps []vcr_v1alpha1.RequestDispSpec) error {
	for _, disp := range reqDisps {
		if disp.Disposition.Action == vcr_v1alpha1.RecvSynth &&
			disp.Disposition.Status == nil {

			return fmt.Errorf("status not set for request " +
				"disposition synth")
		}
		for _, cond := range disp.Conditions {
			if len(cond.Values) == 0 && cond.Count == nil &&
				cond.Compare != vcr_v1alpha1.Exists &&
				cond.Compare != vcr_v1alpha1.NotExists {
				return fmt.Errorf("no values or count set for "+
					"request disposition condition "+
					"(comparand %s)", cond.Comparand)
			}
			if len(cond.Values) != 0 && cond.Count != nil {
				return fmt.Errorf("both values and count set "+
					"for request disposition condition "+
					"(comparand %s)", cond.Comparand)
			}
			if len(cond.Values) > 0 {
				switch cond.Compare {
				case vcr_v1alpha1.Greater,
					vcr_v1alpha1.GreaterEqual,
					vcr_v1alpha1.Less,
					vcr_v1alpha1.LessEqual:
					return fmt.Errorf("illegal compare "+
						"(comparand %s, compare %s)",
						cond.Comparand, cond.Compare)
				}
				switch cond.Comparand {
				case "req.esi_level", "req.restarts":
					return fmt.Errorf("illegal comparison "+
						"(comparand %s, values %v)",
						cond.Comparand, cond.Values)
				}
			}
			if cond.Count != nil {
				switch cond.Compare {
				case vcr_v1alpha1.Match,
					vcr_v1alpha1.NotMatch,
					vcr_v1alpha1.Prefix,
					vcr_v1alpha1.NotPrefix,
					vcr_v1alpha1.Exists,
					vcr_v1alpha1.NotExists:
					return fmt.Errorf("illegal compare "+
						"(compare %s, count %d)",
						cond.Compare, *cond.Count)
				}
				err := false
				switch cond.Comparand {
				case "req.url", "req.method",
					"req.proto":
					err = true
				}
				if strings.HasPrefix(cond.Comparand, "req.http") {
					err = true
				}
				if err {
					return fmt.Errorf("illegal comparison "+
						"(comparand %s, count %d)",
						cond.Comparand, *cond.Count)
				}
			}
		}
	}
	return nil
}

func configComparison(cmp vcr_v1alpha1.CompareType) (vcl.CompareType, bool) {
	switch cmp {
	case vcr_v1alpha1.Equal:
		return vcl.Equal, false
	case vcr_v1alpha1.NotEqual:
		return vcl.Equal, true
	case vcr_v1alpha1.Match:
		return vcl.Match, false
	case vcr_v1alpha1.NotMatch:
		return vcl.Match, true
	case vcr_v1alpha1.Prefix:
		return vcl.Prefix, false
	case vcr_v1alpha1.NotPrefix:
		return vcl.Prefix, true
	case vcr_v1alpha1.Exists:
		return vcl.Exists, false
	case vcr_v1alpha1.NotExists:
		return vcl.Exists, true
	case vcr_v1alpha1.Greater:
		return vcl.Greater, false
	case vcr_v1alpha1.GreaterEqual:
		return vcl.GreaterEqual, false
	case vcr_v1alpha1.Less:
		return vcl.Less, false
	case vcr_v1alpha1.LessEqual:
		return vcl.LessEqual, false
	default:
		return vcl.Equal, false
	}
}

func configMatchFlags(flags vcr_v1alpha1.MatchFlagsType) vcl.MatchFlagsType {
	vclFlags := vcl.MatchFlagsType{
		UTF8:         flags.UTF8,
		PosixSyntax:  flags.PosixSyntax,
		LongestMatch: flags.LongestMatch,
		Literal:      flags.Literal,
		NeverCapture: flags.NeverCapture,
		PerlClasses:  flags.PerlClasses,
		WordBoundary: flags.WordBoundary,
	}
	if flags.MaxMem != nil && *flags.MaxMem != 0 {
		vclFlags.MaxMem = *flags.MaxMem
	}
	if flags.CaseSensitive == nil {
		vclFlags.CaseSensitive = true
	} else {
		vclFlags.CaseSensitive = *flags.CaseSensitive
	}
	switch flags.Anchor {
	case vcr_v1alpha1.None:
		vclFlags.Anchor = vcl.None
	case vcr_v1alpha1.Start:
		vclFlags.Anchor = vcl.Start
	case vcr_v1alpha1.Both:
		vclFlags.Anchor = vcl.Both
	default:
		vclFlags.Anchor = vcl.None
	}
	return vclFlags
}

func reqConds2vclConds(reqConds []vcr_v1alpha1.ReqCondition) []vcl.Condition {
	vclConds := make([]vcl.Condition, len(reqConds))
	for i, reqCond := range reqConds {
		vclCond := vcl.Condition{
			Comparand: reqCond.Comparand,
		}
		if len(reqCond.Values) > 0 {
			vclCond.Values = make([]string, len(reqCond.Values))
			copy(vclCond.Values, reqCond.Values)
		}
		if reqCond.Count != nil {
			count := uint(*reqCond.Count)
			vclCond.Count = &count
		}
		vclCond.Compare, vclCond.Negate =
			configComparison(reqCond.Compare)
		if reqCond.MatchFlags != nil {
			vclCond.MatchFlags = configMatchFlags(
				*reqCond.MatchFlags)
		} else {
			vclCond.MatchFlags.CaseSensitive = true
		}
		vclConds[i] = vclCond
	}
	return vclConds
}

func getHashAlgo(digest string) vcl.HashAlgo {
	switch digest {
	case "CRC32":
		return vcl.Crc32
	case "ICRC32":
		return vcl.ICrc32
	case "MD5":
		return vcl.MD5
	case "RS":
		return vcl.RS
	case "SHA1":
		return vcl.Sha1
	case "SHA224":
		return vcl.Sha224
	case "SHA384":
		return vcl.Sha384
	case "SHA512":
		return vcl.Sha512
	case "SHA3_224":
		return vcl.Sha3_224
	case "SHA3_256":
		return vcl.Sha3_256
	case "SHA3_512":
		return vcl.Sha3_512
	default:
		panic("Illegal digest enum")
	}
}

func (worker *NamespaceWorker) configSharding(spec *vcl.Spec,
	vcfg *vcr_v1alpha1.VarnishConfig, svc *api_v1.Service) update.Status {

	if vcfg.Spec.SelfSharding == nil {
		worker.log.Tracef("No cluster shard configuration for Service "+
			"%s/%s", svc.Namespace, svc.Name)
		return update.MakeNoop("")
	}

	worker.log.Tracef("Set cluster shard configuration for Service %s/%s",
		svc.Namespace, svc.Name)

	endps, err := worker.getAllServiceEndpoints(svc)
	if err != nil {
		return IncompleteIfNotFound(
			err, "Error getting endpoints for service %s/%s: %v",
			svc.Namespace, svc.Name, err)
	}
	if endps == nil || len(endps) == 0 {
		return update.MakeIncomplete(
			"could not find endpoints for service: %s/%s",
			svc.Namespace, svc.Name)
	}
	worker.log.Tracef("Endpoints for shard configuration: %+v", endps)

	// Populate spec.ShardCluster.Nodes with Pod names and the http endpoint
	for _, eps := range endps {
		for _, endpSubset := range eps.Subsets {
			httpPort := int32(0)
			for _, port := range endpSubset.Ports {
				if port.Name == "http" {
					httpPort = port.Port
					break
				}
			}
			if httpPort == 0 {
				continue
			}
			addresses := append(endpSubset.Addresses,
				endpSubset.NotReadyAddresses...)
			for _, addr := range addresses {
				node := vcl.Service{
					Addresses: make([]vcl.Address, 1),
				}
				ns, name := getTargetPod(addr)
				if ns == "" || name == "" {
					return update.MakeFatal(
						"Pod namespace or name not "+
							"known for Endpoint "+
							"%s/%s ip=%s",
						eps.Namespace, eps.Name,
						addr.IP)
				}
				node.Name = eps.Name
				node.Addresses[0].PodNamespace = ns
				node.Addresses[0].PodName = name
				node.Addresses[0].IP = addr.IP
				node.Addresses[0].Port = httpPort
				spec.ShardCluster.Nodes =
					append(spec.ShardCluster.Nodes, node)
			}
		}
	}
	if len(spec.ShardCluster.Nodes) <= 1 {
		worker.log.Warnf(
			"Sharding requested, but %d endpoint addresses found "+
				"for service %s/%s",
			len(spec.ShardCluster.Nodes), svc.Namespace, svc.Name)
	}
	worker.log.Tracef("Node configuration for self-sharding in Service "+
		"%s/%s: %+v", svc.Namespace, svc.Name, spec.ShardCluster.Nodes)

	cfgSpec := vcfg.Spec.SelfSharding
	spec.ShardCluster.Probe = getVCLProbe(cfgSpec.Probe)
	if cfgSpec.Max2ndTTL != "" {
		spec.ShardCluster.MaxSecondaryTTL = cfgSpec.Max2ndTTL
	} else {
		spec.ShardCluster.MaxSecondaryTTL = defMax2ndTTL
	}

	spec.ShardCluster.Rules = make([]vcl.ShardRule, len(cfgSpec.Rules))
	for i, rule := range cfgSpec.Rules {
		vclRule := vcl.ShardRule{
			Conditions:  reqConds2vclConds(rule.Conditions),
			PrimaryOnly: rule.Sharding.PrimaryOnly,
			DefaultKey:  rule.Sharding.DefaultKey,
			By:          vcl.ByHash,
		}
		if rule.Sharding.Digest != "" &&
			rule.Sharding.Digest != "SHA256" {
			vclRule.By = vcl.Blob
			vclRule.Algo = getHashAlgo(rule.Sharding.Digest)
			if rule.Sharding.Key != "" {
				vclRule.Key = rule.Sharding.Key
			} else {
				vclRule.Key = "req.url"
			}
		} else if rule.Sharding.Key != "" {
			vclRule.Key = rule.Sharding.Key
			if rule.Sharding.Key == "req.url" {
				vclRule.By = vcl.URL
			} else if strings.HasPrefix(vclRule.Key, cookieKeyPfx) {
				vclRule.By = vcl.Cookie
				vclRule.Key = strings.TrimPrefix(
					vclRule.Key, cookieKeyPfx)
			} else {
				vclRule.By = vcl.Key
			}
		}
		spec.ShardCluster.Rules[i] = vclRule
	}

	worker.log.Tracef("Spec configuration for self-sharding in Service "+
		"%s/%s: %+v", svc.Namespace, svc.Name, spec.ShardCluster)
	return update.MakeSuccess("")
}

func (worker *NamespaceWorker) configVikingTLS(spec *haproxy.DefaultsSpec,
	tlsCfg *vcr_v1alpha1.VikingTLSSpec) {

	if tlsCfg == nil || tlsCfg.Global == nil ||
		tlsCfg.Global.Timeout == nil {
		return
	}
	spec.Timeouts.Connect = tlsCfg.Global.Timeout.Connect
	spec.Timeouts.Client = tlsCfg.Global.Timeout.Client
	spec.Timeouts.Server = tlsCfg.Global.Timeout.Server
}

func (worker *NamespaceWorker) syncVcfg(key string) update.Status {
	worker.log.Infof("Syncing VarnishConfig: %s/%s", worker.namespace, key)
	vcfg, err := worker.vcfg.Get(key)
	if err != nil {
		return IncompleteIfNotFound(err, "%v", err)
	}
	worker.log.Tracef("VarnishConfig %s/%s: %+v", vcfg.Namespace,
		vcfg.Name, vcfg)

	if len(vcfg.Spec.Services) == 0 {
		// CRD validation should prevent this.
		syncCounters.WithLabelValues(worker.namespace, "VarnishConfig",
			"Ignore").Inc()
		return update.MakeFatal(
			"VarnishConfig %s/%s: no services defined",
			vcfg.Namespace, vcfg.Name)
	}

	if vcfg.Spec.SelfSharding != nil {
		if err = validateProbe(vcfg.Spec.SelfSharding.Probe); err != nil {
			return update.MakeFatal(
				"VarnishConfig %s/%s invalid sharding spec: %v",
				vcfg.Namespace, vcfg.Name, err)
		}
	}
	if err = validateRewrites(vcfg.Spec.Rewrites); err != nil {
		return update.MakeFatal("%v", err)
	}
	if err = validateReqDisps(vcfg.Spec.ReqDispositions); err != nil {
		return update.MakeFatal("%v", err)
	}

	return worker.enqueueIngsForVcfg(vcfg)
}

func (worker *NamespaceWorker) addVcfg(key string) update.Status {
	return worker.syncVcfg(key)
}

func (worker *NamespaceWorker) updateVcfg(key string) update.Status {
	return worker.syncVcfg(key)
}

func (worker *NamespaceWorker) deleteVcfg(obj interface{}) update.Status {
	vcfg, ok := obj.(*vcr_v1alpha1.VarnishConfig)
	if !ok || vcfg == nil {
		return update.MakeNoop(
			"Delete VarnishConfig: not found: %v", obj)
	}
	worker.log.Infof("Deleting VarnishConfig: %s/%s", vcfg.Namespace,
		vcfg.Name)
	return worker.enqueueIngsForVcfg(vcfg)
}
