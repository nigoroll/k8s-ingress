/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"fmt"
	"strings"
	"text/template"
)

const rewriteTmplSrc = `
import re2;
import selector;

{{range $i, $r := .Rewrites -}}
{{if needsMatcher $r -}}
sub vcl_init {
	new {{rewrName $i}} = {{vmod $r.Compare}}.set({{rewrFlags $r}});
	{{- range $rule := $r.Rules}}
	{{rewrName $i}}.add("{{$rule.Value}}", string="{{$rule.Rewrite}}"
		{{- if needsSave $r}}, save=true{{end -}}
		{{- if needsRegex $r}}, regex="{{saveRegex $r $rule}}"{{end -}}
		{{- if needsNeverCapture $r}}, never_capture=true{{end -}}
		);
	{{- end}}
	{{rewrName $i}}.compile();
}
{{- end}}

sub vcl_{{rewrSub $r}} {
	{{- if needsMatcher $r}}
	if ({{rewrName $i}}.{{match $r.Compare}}({{$r.Source}})) {
		{{- if needsUniqueCheck $r}}
		if ({{rewrName $i}}.nmatches() != 1) {
			std.log({{$r.Source}} + " had " +
				{{rewrName $i}}.nmatches() + " matches");
			return(fail);
		}
		{{- end}}
        {{- end}}
	{{- if rewrMethodDelete $r}}
		unset {{$r.Target}};
	{{- else if rewrMethodAppend $r}}
		set {{$r.Target}} = {{rewrOperand1 $r}} + {{rewrOperand2 $r $i}};
	{{- else if rewrMethodPrepend $r}}
		set {{$r.Target}} = {{rewrOperand2 $r $i}} + {{rewrOperand1 $r}};
	{{- else if rewrMethodReplace $r}}
		set {{$r.Target}} = {{rewrOperand2 $r $i}};
	{{- else}}
		set {{$r.Target}} =
			{{rewrName $i}}.{{rewrOp $r}}({{$r.Source}},
		                       {{rewrName $i}}.string({{rewrSelect $r}})
		                       {{- if needsAll $r}}, all=true{{end -}}
		                       {{- if needsSelectEnum $r -}}
                                       , {{rewrSelect $r}}{{end -}}
			);
	{{- end}}
	{{- if needsMatcher $r}}
	}
	{{- end}}
}
{{- end}}
`

const rewriteTmplName = "rewrite"

func needsMatcher(rewr Rewrite) bool {
	switch rewr.Method {
	case Append, Prepend, Delete, Replace:
		if len(rewr.Rules) == 0 ||
			(len(rewr.Rules) == 1 && rewr.Rules[0].Value == "") {
			return false
		}
		return true
	default:
		return true
	}
}

func needsSave(rewr Rewrite) bool {
	if rewr.Compare != Match {
		return false
	}
	switch rewr.Method {
	case Sub, Suball, RewriteMethod:
		return true
	default:
		return false
	}
}

func rewrName(i int) string {
	return fmt.Sprintf("vk8s_rewrite_%d", i)
}

func rewrFlags(rewr Rewrite) string {
	return matcherFlags(rewr.Compare != Match, rewr.MatchFlags)
}

func rewrSub(rewr Rewrite) string {
	if rewr.VCLSub == Unspecified {
		if strings.HasPrefix(rewr.Target, "resp") ||
			strings.HasPrefix(rewr.Target, "resp") {
			rewr.VCLSub = Deliver
		} else if strings.HasPrefix(rewr.Target, "beresp") ||
			strings.HasPrefix(rewr.Target, "beresp") {
			rewr.VCLSub = BackendResponse
		} else if strings.HasPrefix(rewr.Target, "req") {
			rewr.VCLSub = Recv
		} else {
			rewr.VCLSub = BackendFetch
		}
	}
	switch rewr.VCLSub {
	case Recv:
		return "recv"
	case Pipe:
		return "pipe"
	case Pass:
		return "pass"
	case Hash:
		return "hash"
	case Purge:
		return "purge"
	case Miss:
		return "miss"
	case Hit:
		return "hit"
	case Deliver:
		return "deliver"
	case Synth:
		return "synth"
	case BackendFetch:
		return "backend_fetch"
	case BackendResponse:
		return "backend_response"
	case BackendError:
		return "backend_error"
	default:
		return "__UNKNOWN_VCL_SUB__"
	}
}

func rewrSelect(rewr Rewrite) string {
	if rewr.Select == Unique {
		return ""
	}
	return "select=" + rewr.Select.String()
}

func rewrOperand1(rewr Rewrite) string {
	if len(rewr.Rules) == 0 {
		return rewr.Target
	}
	return rewr.Source
}

func rewrOperand2(rewr Rewrite, i int) string {
	if len(rewr.Rules) == 1 && rewr.Rules[0].Value == "" {
		return `"` + rewr.Rules[0].Rewrite + `"`
	}
	if len(rewr.Rules) > 0 && rewr.Rules[0].Value != "" {
		return rewrName(i) + ".string(" + rewrSelect(rewr) + ")"
	}
	return rewr.Source
}

func rewrOp(rewr Rewrite) string {
	switch rewr.Method {
	case Sub:
		return "sub"
	case Suball:
		if rewr.Compare == Match {
			return "suball"
		}
		return "sub"
	case RewriteMethod:
		return "extract"
	default:
		return "__INVALID_REWRITE_OPERAION__"
	}
}

var rewriteFuncs = template.FuncMap{
	"match":        func(cmp CompareType) string { return match(cmp) },
	"needsMatcher": func(rewr Rewrite) bool { return needsMatcher(rewr) },
	"needsSave":    func(rewr Rewrite) bool { return needsSave(rewr) },
	"rewrFlags":    func(rewr Rewrite) string { return rewrFlags(rewr) },
	"rewrSub":      func(rewr Rewrite) string { return rewrSub(rewr) },
	"rewrOp":       func(rewr Rewrite) string { return rewrOp(rewr) },
	"rewrSelect":   func(rewr Rewrite) string { return rewrSelect(rewr) },
	"rewrName":     func(i int) string { return rewrName(i) },
	"vmod":         func(cmp CompareType) string { return vmod(cmp) },
	"needsAll": func(rewr Rewrite) bool {
		return rewr.Compare != Match && rewr.Method == Suball
	},
	"needsNeverCapture": func(rewr Rewrite) bool {
		return rewr.Compare == Match && rewr.MatchFlags.NeverCapture
	},
	"needsRegex": func(rewr Rewrite) bool {
		return rewr.Compare != Match &&
			(rewr.Method == Sub || rewr.Method == Suball)
	},
	"needsUniqueCheck": func(rewr Rewrite) bool {
		if rewr.Compare == Equal || rewr.Select != Unique {
			return false
		}
		switch rewr.Method {
		case Delete:
			return false
		case Sub, Suball, RewriteMethod:
			return true
		default:
			return len(rewr.Rules) > 0 && rewr.Rules[0].Value != ""
		}
	},
	"needsSelectEnum": func(rewr Rewrite) bool {
		return rewr.Select != Unique
	},
	"rewrMethodAppend": func(rewr Rewrite) bool {
		return rewr.Method == Append
	},
	"rewrMethodPrepend": func(rewr Rewrite) bool {
		return rewr.Method == Prepend
	},
	"rewrMethodDelete": func(rewr Rewrite) bool {
		return rewr.Method == Delete
	},
	"rewrMethodReplace": func(rewr Rewrite) bool {
		return rewr.Method == Replace
	},
	"rewrOperand1": func(rewr Rewrite) string {
		return rewrOperand1(rewr)
	},
	"rewrOperand2": func(rewr Rewrite, i int) string {
		return rewrOperand2(rewr, i)
	},
	"saveRegex": func(rewr Rewrite, rule RewriteRule) string {
		regex := `^\Q` + rule.Value + `\E`
		if rewr.Compare == Prefix {
			return regex
		}
		return regex + "$"
	},
}

var rewriteTmpl = template.Must(template.New(rewriteTmplName).
	Funcs(rewriteFuncs).Parse(rewriteTmplSrc))

// ResetRewriteTmpl sets the VCL template for the VarnishConfig
// rewrite feature to its current "official" value. Invoked on
// TemplateConfig deletion, only needed when devmode is activated for
// the controller.
func ResetRewriteTmpl() {
	rewriteTmpl = template.Must(template.New(rewriteTmplName).
		Funcs(rewriteFuncs).Parse(rewriteTmplSrc))
}

// SetRewriteTmpl parses src as a text/template, using the FuncMap
// defined for the rewrite template, which is used to generate VCL for
// the VarnishConfig rewrite feature. On success, the rewrite template
// is replaced. If the parse fails, then the error is returned and the
// rewrite template is unchanged.
//
// Only used when devmode is activated for the controller.
func SetRewriteTmpl(src string) error {
	newTmpl, err := template.New(rewriteTmplName).Funcs(rewriteFuncs).
		Parse(src)
	if err != nil {
		return err
	}
	rewriteTmpl = newTmpl
	return nil
}
