/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"bytes"
	"io/ioutil"
	"path/filepath"
	"testing"
	"text/template"

	"github.com/andreyvit/diff"
)

func goldBytes(goldfile string) ([]byte, error) {
	goldpath := filepath.Join("testdata", goldfile)
	gold, err := ioutil.ReadFile(goldpath)
	if err != nil {
		return nil, err
	}
	return gold, nil
}

func cmpGold(got []byte, goldfile string) (bool, error) {
	gold, err := goldBytes(goldfile)
	if err != nil {
		return false, err
	}
	return bytes.Equal(got, gold), nil
}

func diffGold(got bytes.Buffer, goldfile string) (string, error) {
	gold, err := goldBytes(goldfile)
	if err != nil {
		return "", err
	}
	return diff.LineDiff(string(gold), got.String()), nil
}

func templateTest(
	t *testing.T, tmpl *template.Template, spec interface{}, gold string,
) {
	var buf bytes.Buffer
	if err := tmpl.Execute(&buf, spec); err != nil {
		t.Fatal("Execute():", err)
	}
	ok, err := cmpGold(buf.Bytes(), gold)
	if err != nil {
		t.Fatalf("Reading %s: %v", gold, err)
	}
	if !ok {
		t.Errorf("Generated VCL does not match gold file: %s", gold)
		if testing.Verbose() {
			if msg, err := diffGold(buf, gold); err == nil {
				t.Logf("diff:\n%s", msg)
			} else {
				t.Logf("Generated:\n%s", buf.String())
			}
		}
	}
}

var teaSvc = Service{
	Name: "tea-svc",
	Addresses: []Address{
		{
			PodNamespace: "default",
			PodName:      "tea-5798f99dc5-5wj8n",
			IP:           "192.0.2.1",
			Port:         80,
		},
		{
			PodNamespace: "default",
			PodName:      "tea-5798f99dc5-hn27l",
			IP:           "192.0.2.2",
			Port:         80,
		},
		{
			PodNamespace: "default",
			PodName:      "tea-5798f99dc5-5wj8n",
			IP:           "192.0.2.3",
			Port:         80,
		},
	},
}

var coffeeSvc = Service{
	Name: "coffee-svc",
	Addresses: []Address{
		{
			PodNamespace: "default",
			PodName:      "coffee-6b9f5c47d7-bdt68",
			IP:           "192.0.2.4",
			Port:         80,
		},
		{
			PodNamespace: "default",
			PodName:      "coffee-6b9f5c47d7-l5zvl",
			IP:           "192.0.2.5",
			Port:         80,
		},
	},
}

var cafeSpec = Spec{
	DefaultService: Service{},
	Rules: []Rule{{
		Host: "cafe.example.com",
		PathMap: map[PathKey]Service{
			{
				Path: "/tea",
				Type: PathPrefix,
			}: teaSvc,
			{
				Path: "/coffee",
				Type: PathPrefix,
			}: coffeeSvc,
		},
	}},
	IntSvcs: map[string]Service{
		"tea-svc":    teaSvc,
		"coffee-svc": coffeeSvc,
	},
}

func TestIngressTemplate(t *testing.T) {
	templateTest(t, ingressTmpl, cafeSpec, "ingressrule.golden")
}

var auths = Spec{
	Auths: []Auth{
		{
			Realm:  "foo",
			Status: Basic,
			Credentials: []string{
				"QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
				"QWxhZGRpbjpPcGVuU2VzYW1l",
			},
		},
		{
			Realm:  "bar",
			Status: Proxy,
			Credentials: []string{
				"Zm9vOmJhcg==",
				"YmF6OnF1dXg=",
			},
			UTF8: false,
		},
		{
			Realm:  "baz",
			Status: Basic,
			Credentials: []string{
				"dXNlcjpwYXNzd29yZDE=",
				"bmFtZTpzZWNyZXQ=",
			},
			Conditions: []MatchTerm{{
				Comparand: "req.http.Host",
				Value:     `baz.com`,
				Compare:   Equal,
			}},
			UTF8: true,
		},
		{
			Realm:  "quux",
			Status: Proxy,
			Credentials: []string{
				"YmVudXR6ZXI6Z2VoZWlt",
				"QWxiZXJ0IEFkZGluOm9wZW4gc2V6IG1l",
			},
			Conditions: []MatchTerm{{
				Comparand: "req.url",
				Value:     "^/baz/quux",
				Compare:   Match,
			}},
			UTF8: true,
		},
		{
			Realm:  "urlhost",
			Status: Basic,
			Credentials: []string{
				"dXJsOmhvc3Q=",
				"YWRtaW46c3VwZXJwb3dlcnM=",
			},
			Conditions: []MatchTerm{
				{
					Comparand: "req.http.Host",
					Value:     "url.regex.org",
					Compare:   Equal,
				},
				{
					Comparand: "req.url",
					Value:     "^/secret/path",
					Compare:   Match,
				},
			},
		},
	},
}

func TestAuthTemplate(t *testing.T) {
	templateTest(t, authTmpl, auths, "auth.golden")
}

var acls = Spec{
	ACLs: []ACL{
		{
			Name:       "man_vcl_example",
			Comparand:  "client.ip",
			FailStatus: 403,
			Whitelist:  true,
			Addresses: []ACLAddress{
				{
					Addr:     "localhost",
					MaskBits: 255,
					Negate:   false,
				},
				{
					Addr:     "192.0.2.0",
					MaskBits: 24,
					Negate:   false,
				},
				{
					Addr:     "192.0.2.23",
					MaskBits: 255,
					Negate:   true,
				},
			},
			Conditions: []MatchTerm{
				{
					Comparand: "req.http.Host",
					Compare:   Equal,
					Value:     "cafe.example.com",
				},
				{
					Comparand: "req.url",
					Compare:   Match,
					Value:     `^/coffee(/|$)`,
				},
			},
		},
		{
			Name:       "wikipedia_example",
			Comparand:  "server.ip",
			FailStatus: 404,
			Whitelist:  false,
			Addresses: []ACLAddress{
				{
					Addr:     "192.168.100.14",
					MaskBits: 24,
					Negate:   false,
				},
				{
					Addr:     "192.168.100.0",
					MaskBits: 22,
					Negate:   false,
				},
				{
					Addr:     "2001:db8::",
					MaskBits: 48,
					Negate:   false,
				},
			},
			Conditions: []MatchTerm{
				{
					Comparand: "req.http.Host",
					Compare:   Equal,
					Negate:    true,
					Value:     "cafe.example.com",
				},
				{
					Comparand: "req.url",
					Compare:   Match,
					Negate:    true,
					Value:     `^/tea(/|$)`,
				},
			},
		},
		{
			Name:       "private4",
			Comparand:  "req.http.X-Real-IP",
			FailStatus: 403,
			Whitelist:  true,
			Addresses: []ACLAddress{
				{
					Addr:     "10.0.0.0",
					MaskBits: 24,
					Negate:   false,
				},
				{
					Addr:     "172.16.0.0",
					MaskBits: 12,
					Negate:   false,
				},
				{
					Addr:     "192.168.0.0",
					MaskBits: 16,
					Negate:   false,
				},
			},
			Conditions: []MatchTerm{},
		},
		{
			Name:       "rfc5737",
			Comparand:  "xff-first",
			FailStatus: 403,
			Whitelist:  true,
			Addresses: []ACLAddress{
				{
					Addr:     "192.0.2.0",
					MaskBits: 24,
					Negate:   false,
				},
				{
					Addr:     "198.51.100.0",
					MaskBits: 24,
					Negate:   false,
				},
				{
					Addr:     "203.0.113.0",
					MaskBits: 24,
					Negate:   false,
				},
			},
			Conditions: []MatchTerm{},
		},
		{
			Name:       "local",
			Comparand:  "xff-2ndlast",
			FailStatus: 403,
			Whitelist:  true,
			Addresses: []ACLAddress{
				{
					Addr:     "127.0.0.0",
					MaskBits: 8,
					Negate:   false,
				},
				{
					Addr:     "::1",
					MaskBits: 255,
					Negate:   false,
				},
			},
			Conditions: []MatchTerm{},
		},
	},
}

func TestAclTemplate(t *testing.T) {
	templateTest(t, aclTmpl, acls, "acl.golden")
}

var aclResultHdr = Spec{
	ACLs: []ACL{{
		Name:       "man_vcl_example",
		Comparand:  "client.ip",
		FailStatus: 403,
		Whitelist:  true,
		Addresses: []ACLAddress{
			{
				Addr:     "localhost",
				MaskBits: 255,
				Negate:   false,
			},
			{
				Addr:     "192.0.2.0",
				MaskBits: 24,
				Negate:   false,
			},
			{
				Addr:     "192.0.2.23",
				MaskBits: 255,
				Negate:   true,
			},
		},
		ResultHdr: ResultHdrType{
			Header:  "req.http.ACL-Whitelist",
			Success: "pass",
			Failure: "fail",
		},
	}},
}

func TestAclResultHeader(t *testing.T) {
	templateTest(t, aclTmpl, aclResultHdr, "acl_result_hdr.golden")
}

var aclNoFail = Spec{
	ACLs: []ACL{{
		Name:       "acl_no_fail",
		Comparand:  "client.ip",
		FailStatus: 0,
		Whitelist:  true,
		Addresses: []ACLAddress{
			{
				Addr:     "192.0.2.0",
				MaskBits: 24,
				Negate:   false,
			},
			{
				Addr:     "198.51.100.0",
				MaskBits: 24,
				Negate:   false,
			},
			{
				Addr:     "203.0.113.0",
				MaskBits: 24,
				Negate:   false,
			},
		},
		ResultHdr: ResultHdrType{
			Header:  "req.http.ACL-Whitelist",
			Success: "pass",
			Failure: "fail",
		},
	}},
}

func TestAclNoFail(t *testing.T) {
	templateTest(t, aclTmpl, aclNoFail, "acl_no_fail.golden")
}

var customVCLSpec = Spec{
	DefaultService: Service{},
	Rules: []Rule{{
		Host: "cafe.example.com",
		PathMap: map[PathKey]Service{
			{
				Path: "/tea",
				Type: PathPrefix,
			}: teaSvc,
			{
				Path: "/coffee",
				Type: PathPrefix,
			}: coffeeSvc,
		},
	}},
	IntSvcs: map[string]Service{
		"tea-svc":    teaSvc,
		"coffee-svc": coffeeSvc,
	},
	VCL: `sub vcl_deliver {
	set resp.http.Hello = "world";
}`,
}

func TestCustomVCL(t *testing.T) {
	gold := "custom_vcl.golden"

	vcl, err := customVCLSpec.GetSrc()
	if err != nil {
		t.Fatal("GetSrc():", err)
	}

	ok, err := cmpGold([]byte(vcl), gold)
	if err != nil {
		t.Fatalf("Reading %s: %v", gold, err)
	}
	if !ok {
		t.Errorf("Generated VCL does not match gold file: %s", gold)
		if testing.Verbose() {
			buf := bytes.NewBufferString(vcl)
			if msg, err := diffGold(*buf, gold); err == nil {
				t.Logf("diff:\n%s", msg)
			} else {
				t.Logf("Generated:\n%s", vcl)
			}
		}
	}
}

var teaSvcProbeDir = Service{
	Name: "tea-svc",
	Addresses: []Address{
		{
			IP:   "192.0.2.1",
			Port: 80,
		},
		{
			IP:   "192.0.2.2",
			Port: 80,
		},
		{
			IP:   "192.0.2.3",
			Port: 80,
		},
	},
	HostHeader:          "tea.svc.org",
	ConnectTimeout:      "1s",
	FirstByteTimeout:    "2s",
	BetweenBytesTimeout: "2s",
	MaxConnections:      200,
	ProxyHeader:         1,
	Probe: &Probe{
		URL:         "/healthz",
		ExpResponse: 204,
		Timeout:     "5s",
		Interval:    "5s",
		Initial:     "2",
		Window:      "8",
		Threshold:   "3",
	},
	Director: &Director{
		Type:   Shard,
		Rampup: "5m",
		Warmup: 0.5,
	},
}

var coffeeSvcProbeDir = Service{
	Name: "coffee-svc",
	Addresses: []Address{
		{
			IP:   "192.0.2.4",
			Port: 80,
		},
		{
			IP:   "192.0.2.5",
			Port: 80,
		},
	},
	HostHeader:          "coffee.svc.org",
	ConnectTimeout:      "3s",
	FirstByteTimeout:    "2s",
	BetweenBytesTimeout: "1s",
	ProxyHeader:         2,
	Probe: &Probe{
		Request: []string{
			"GET /healthz HTTP/1.1",
			"Host: coffee.svc.org",
			"Connection: close",
		},
		Timeout:   "4s",
		Interval:  "4s",
		Initial:   "1",
		Window:    "7",
		Threshold: "2",
	},
	Director: &Director{
		Type: Random,
	},
}

var milkSvcProbeDir = Service{
	Name: "milk-svc",
	Addresses: []Address{
		{
			IP:   "192.0.2.6",
			Port: 80,
		},
		{
			IP:   "192.0.2.7",
			Port: 80,
		},
	},
	HostHeader:       "milk.svc.org",
	FirstByteTimeout: "3s",
	Probe: &Probe{
		Timeout:   "5s",
		Interval:  "5s",
		Window:    "3",
		Threshold: "2",
	},
	Director: &Director{
		Type: RoundRobin,
	},
}

var cafeProbeDir = Spec{
	DefaultService: Service{},
	Rules: []Rule{{
		Host: "cafe.example.com",
		PathMap: map[PathKey]Service{
			{
				Path: "/tea",
				Type: PathExact,
			}: teaSvcProbeDir,
			{
				Path: "/coffee",
				Type: PathPrefix,
			}: coffeeSvcProbeDir,
			{
				Path: "/milk",
				Type: PathImplSpecific,
			}: milkSvcProbeDir,
		},
	}},
	IntSvcs: map[string]Service{
		"tea-svc":    teaSvcProbeDir,
		"coffee-svc": coffeeSvcProbeDir,
		"milk-svc":   milkSvcProbeDir,
	},
}

func TestBackendConfig(t *testing.T) {
	templateTest(t, ingressTmpl, cafeProbeDir, "backendcfg.golden")
}

func TestViaBackend(t *testing.T) {
	teaSvc.Via = true
	cafeSpec.IntSvcs["tea-svc"] = teaSvc

	templateTest(t, ingressTmpl, cafeSpec, "via_backend.golden")

	teaSvc.Authority = new(string)
	*teaSvc.Authority = "www.tea.org"
	cafeSpec.IntSvcs["tea-svc"] = teaSvc

	templateTest(t, ingressTmpl, cafeSpec, "via_authority_backend.golden")
}

// Code boilerplate for writing the golden file.
// import ioutils
// func TestXXX(t *testing.T) {
// gold := "XXX.golden"
// var buf bytes.Buffer

// if err := xxxTmpl.Execute(&buf, spec); err != nil {
// 	t.Fatal("Execute():", err)
// }

// if err := ioutil.WriteFile("testdata/"+gold, buf.Bytes(), 0644); err != nil {
// 		t.Fatal("WriteFile():", err)
// 	}
// if testing.Verbose() {
// 	t.Logf("Generated: %s", buf.String())
// }
// }
