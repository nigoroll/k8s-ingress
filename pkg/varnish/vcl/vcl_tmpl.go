/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"sort"
	"strings"
	"text/template"
)

const ingTmplSrc = `vcl 4.1;

import std;
import directors;
import re2;
import dynamic;
import selector;

include "bogo_backend.vcl";

{{- define "ProbeDef"}}
{{- range $name, $svc := .}}
{{- if $svc.Probe}}
{{with $svc.Probe}}
probe {{probeName $name}} {
{{- if ne .URL ""}}
	.url = "{{.URL}}";
{{- else if .Request}}
	.request =
{{- range $reqLine := .Request}}
		"{{$reqLine}}"
{{- end}}
		;
{{- end}}
{{- if .ExpResponse}}
	.expected_response = {{.ExpResponse}};
{{- end}}
{{- if .Timeout}}
	.timeout = {{.Timeout}};
{{- end}}
{{- if .Interval}}
	.interval = {{.Interval}};
{{- end}}
{{- if .Initial}}
	.initial = {{.Initial}};
{{- end}}
{{- if .Window}}
	.window = {{.Window}};
{{- end}}
{{- if .Threshold}}
	.threshold = {{.Threshold}};
{{- end}}
}
{{- end}}
{{- end}}
{{- end}}
{{- end}}
{{- template "ProbeDef" .IntSvcs -}}
{{- template "ProbeDef" .ExtSvcs}}
{{- if needsVia .IntSvcs .ExtSvcs }}
backend vk8s_via { .path = "/run/offload/onload.sock"; }
{{- end }}

{{range $name, $svc := .IntSvcs -}}
{{range $addr := $svc.Addresses -}}
backend {{backendName $svc $addr}} {
	.host = "{{$addr.IP}}";
	.port = "{{$addr.Port}}";
{{- with $svc}}
{{- if .HostHeader}}
	.host_header = "{{.HostHeader}}";
{{- end}}
{{- if .ConnectTimeout}}
	.connect_timeout = {{.ConnectTimeout}};
{{- end}}
{{- if .FirstByteTimeout}}
	.first_byte_timeout = {{.FirstByteTimeout}};
{{- end}}
{{- if .BetweenBytesTimeout}}
	.between_bytes_timeout = {{.BetweenBytesTimeout}};
{{- end}}
{{- if .ProxyHeader}}
	.proxy_header = {{.ProxyHeader}};
{{- end}}
{{- if .MaxConnections}}
	.max_connections = {{.MaxConnections}};
{{- end}}
{{- if .Probe}}
	.probe = {{probeName $name}};
{{- end}}
{{- if .Via}}
	.via = vk8s_via;
{{- end}}
{{- if .Authority}}
	.authority = "{{.Authority}}";
{{- end}}
{{- end}}
}
{{end -}}
{{end}}

sub vcl_init {

{{- range $name, $svc := .IntSvcs}}
	{{- $dirType := dirType $svc}}
	new {{dirName $svc}} = directors.{{$dirType}}();
	{{- range $addr := $svc.Addresses}}
	{{dirName $svc}}.add_backend({{backendName $svc $addr}}
		{{- if eq $dirType "random"}}
		, 1.0
		{{- end}}
		);
	{{- end}}
	{{- if eq $dirType "shard"}}
	{{- if $svc.Director.Warmup}}
	{{dirName $svc}}.set_warmup({{$svc.Director.Warmup}});
	{{- end}}
	{{- if $svc.Director.Rampup}}
	{{dirName $svc}}.set_rampup({{$svc.Director.Rampup}});
	{{- end}}
	{{dirName $svc}}.reconfigure();
	{{- end}}
{{end}}
{{- range $name, $svc := .ExtSvcs}}
	new {{resolverName $svc}} = dynamic.resolver();
	{{resolverName $svc}}.set_resolution_type(STUB);
	new {{dirName $svc}} = dynamic.director(
		resolver = {{resolverName $svc}}.use()
		{{- with $svc}}
		{{- if .NameTTL }}
			{{- if .NameTTL.TTL }}
		, ttl = {{ .NameTTL.TTL }}
			{{- end }}
			{{- if .NameTTL.From }}
		, ttl_from = {{ .NameTTL.From }}
			{{- end }}
		{{- else }}
		, ttl_from = dns
		{{- end }}
		{{- if .DNSRetryDelay }}
		, retry_after = {{ .DNSRetryDelay }}
		{{- end }}
		{{- if .ExternalPort}}
		, port = "{{.ExternalPort}}"
		{{- end}}
		{{- if .HostHeader}}
		, host_header = "{{.HostHeader}}"
		{{- end}}
		{{- if .ConnectTimeout}}
		, connect_timeout = {{.ConnectTimeout}}
		{{- end}}
		{{- if .FirstByteTimeout}}
		, first_byte_timeout = {{.FirstByteTimeout}}
		{{- end}}
		{{- if .BetweenBytesTimeout}}
		, between_bytes_timeout = {{.BetweenBytesTimeout}}
		{{- end}}
		{{- if .DomainUsageTimeout}}
		, domain_usage_timeout = {{.DomainUsageTimeout}}
		{{- end}}
		{{- if .FirstLookupTimeout}}
		, first_lookup_timeout = {{.FirstLookupTimeout}}
		{{- end}}
		{{- if .ProxyHeader}}
		, proxy_header = {{.ProxyHeader}}
		{{- end}}
		{{- if .MaxConnections}}
		, max_connections = {{.MaxConnections}}
		{{- end}}
		{{- if .Probe}}
		, probe = {{probeName $name}}
		{{- end}}
		{{- if .Via}}
		, via = vk8s_via
		{{- end}}
		{{- end}}
	);
	{{- if .ResolverIdleTimeout}}
	{{resolverName $svc}}.set_idle_timeout({{.ResolverIdleTimeout}});
	{{- end }}
	{{- if .ResolverTimeout}}
	{{resolverName $svc}}.set_timeout({{.ResolverTimeout}});
	{{- end }}
	{{- if .MaxDNSQueries}}
	{{resolverName $svc}}.set_limit_outstanding_queries({{.MaxDNSQueries}});
	{{- end }}
	{{- if not .FollowDNSRedirects}}
	{{resolverName $svc}}.set_follow_redirects(REDIRECTS_DO_NOT_FOLLOW);
	{{- end }}
{{end}}
{{- range $rule := .Rules}}
	{{ if hasExact $rule -}}
	new {{exactMatcher $rule}} = selector.set();
	{{- end}}
	{{ if hasPrefix $rule -}}
	new {{pfxMatcher $rule}} = re2.set(anchor=start);
	{{- end}}
	{{ if hasImplSpec $rule -}}
	new {{regexMatcher $rule}} = re2.set();
	{{- end}}

	{{- range $pathKey, $svc := $rule.PathMap}}
	{{- if isExact $pathKey.Type }}
	{{exactMatcher $rule}}.add("{{$pathKey.Path}}",
		{{- if $svc.ExternalName}}
		backend={{dirName $svc}}.backend("{{$svc.ExternalName}}"));
		{{- else}}
		backend={{dirName $svc}}.backend());
		{{- end}}
	{{- else if isImplSpec $pathKey.Type }}
	{{regexMatcher $rule}}.add("{{$pathKey.Path}}",
		{{- if $svc.ExternalName}}
		backend={{dirName $svc}}.backend("{{$svc.ExternalName}}"));
		{{- else}}
		backend={{dirName $svc}}.backend());
		{{- end}}
	{{- end}}
	{{- end}}
	{{- range $path := pfxSorted $rule.PathMap}}
	{{pfxMatcher $rule}}.add("{{pfxPattern $path}}",
		{{- $svc := pfxSvc $rule.PathMap $path }}
		{{- if $svc.ExternalName}}
		backend={{dirName $svc}}.backend("{{$svc.ExternalName}}"));
		{{- else}}
		backend={{dirName $svc}}.backend());
		{{- end}}
	{{- end}}
{{end -}}
}

{{ range $rule := .Rules -}}
sub {{hostMangle $rule.Host}}_match {
	{{- if hasExact $rule}}
	if ({{exactMatcher $rule}}.match(bereq.url)) {
		set bereq.backend = {{exactMatcher $rule}}.backend();
	}
	{{end -}}
	{{- if hasPrefix $rule }}
	{{ ifelsif $rule 1 }} ({{pfxMatcher $rule}}.match(bereq.url)) {
		set bereq.backend = {{pfxMatcher $rule}}.backend(select=LAST);
	}
	{{end -}}
	{{- if hasImplSpec $rule}}
	{{ ifelsif $rule 2 }} ({{regexMatcher $rule}}.match(bereq.url)) {
		set bereq.backend = {{regexMatcher $rule}}.backend(select=FIRST);
	}
	{{- end}}
}

{{end -}}

{{ if .Rules -}}
sub vcl_init {
	new vk8s_hosts = re2.set(anchor=both);
	{{- range $rule := .Rules}}
	vk8s_hosts.add("{{hostRegex $rule.Host}}(:\d+)?",
	               sub={{hostMangle $rule.Host}}_match);
	{{- end}}
}
{{- end}}

sub vcl_backend_fetch {
	set bereq.backend = vk8s_notfound;
{{- if .Rules}}
	if (vk8s_hosts.match(bereq.http.Host)) {
		call vk8s_hosts.subroutine(select=FIRST);
	}
{{- end}}

	if (bereq.backend == vk8s_notfound) {
{{- if .DefaultService.Name}}
		set bereq.backend = {{dirName .DefaultService}}.backend();
{{- else}}
		return (error(404));
{{- end}}
	}
}
`

const ingTmplName = "vcl"

func dirType(svc Service) string {
	if svc.Director == nil {
		return RoundRobin.String()
	}
	return svc.Director.Type.String()
}

func hostMangle(host string) string {
	if host == "" {
		return mangle("any_host")
	}
	return mangle(strings.Replace(host, ".", "_", -1))
}

func hasPathType(rule Rule, t PathType) bool {
	for pathKey := range rule.PathMap {
		if pathKey.Type == t {
			return true
		}
	}
	return false
}

func hasExact(rule Rule) bool {
	return hasPathType(rule, PathExact)
}

func hasPrefix(rule Rule) bool {
	return hasPathType(rule, PathPrefix)
}

func hasImplSpec(rule Rule) bool {
	return hasPathType(rule, PathImplSpecific)
}

func isPathType(t PathType, val PathType) bool {
	return t == val
}

func matcherName(rule Rule, suffix string) string {
	return hostMangle(rule.Host) + suffix
}

func pfxSorted(pathMap map[PathKey]Service) []string {
	pfxs := []string{}
	for pathKey := range pathMap {
		if pathKey.Type != PathPrefix {
			continue
		}
		pfxs = append(pfxs, pathKey.Path)
	}
	sort.Slice(pfxs, func(i, j int) bool {
		return len(pfxs[i]) < len(pfxs[j])
	})
	return pfxs
}

func pfxSvc(pathMap map[PathKey]Service, path string) Service {
	for pathKey := range pathMap {
		if pathKey.Type != PathPrefix {
			continue
		}
		if pathKey.Path == path {
			return pathMap[pathKey]
		}
	}
	panic("Service for path prefix " + path + "not found")
}

func ifelsif(rule Rule, which int) string {
	switch which {
	case 1:
		if hasExact(rule) {
			return "elsif"
		}
		return "if"
	case 2:
		if hasExact(rule) || hasPrefix(rule) {
			return "elsif"
		}
		return "if"
	default:
		panic("ifelsif illegal param")
	}
}

func hostRegex(host string) string {
	if host == "" {
		return "[^:]+"
	}
	if strings.HasPrefix(host, "*") {
		return `[^.]+\Q` + strings.TrimLeft(host, "*") + `\E`
	}
	return `\Q` + host + `\E`
}

func pfxPattern(path string) string {
	if path == "/" {
		return "/"
	}
	return `\Q` + strings.TrimRight(path, "/") + `\E(/|$)`
}

func needsVia(intSvcs, extSvcs map[string]Service) bool {
	for _, svc := range intSvcs {
		if svc.Via {
			return true
		}
	}
	for _, svc := range extSvcs {
		if svc.Via {
			return true
		}
	}
	return false
}

var vclFuncs = template.FuncMap{
	"plusOne":     func(i int) int { return i + 1 },
	"dirType":     func(svc Service) string { return dirType(svc) },
	"hostRegex":   func(host string) string { return hostRegex(host) },
	"backendName": backendName,
	"hostMangle":  hostMangle,
	"needsVia":    needsVia,
	"pfxPattern":  pfxPattern,
	"pfxSorted":   pfxSorted,
	"pfxSvc":      pfxSvc,
	"ifelsif":     ifelsif,
	"hasExact":    hasExact,
	"hasPrefix":   hasPrefix,
	"hasImplSpec": hasImplSpec,
	"isExact": func(t PathType) bool {
		return isPathType(t, PathExact)
	},
	"isImplSpec": func(t PathType) bool {
		return isPathType(t, PathImplSpecific)
	},
	"exactMatcher": func(rule Rule) string {
		return matcherName(rule, "_exactMatcher")
	},
	"pfxMatcher": func(rule Rule) string {
		return matcherName(rule, "_pfxMatcher")
	},
	"regexMatcher": func(rule Rule) string {
		return matcherName(rule, "_regexMatcher")
	},
	"probeName": func(name string) string {
		return mangle(name + "_probe")
	},
	"dirName": func(svc Service) string {
		return mangle(svc.Name + "_director")
	},
	"resolverName": func(svc Service) string {
		return mangle(svc.Name + "_resolver")
	},
}

var ingressTmpl = template.Must(template.New(ingTmplName).
	Funcs(vclFuncs).Parse(ingTmplSrc))

// ResetIngressTmpl sets the VCL template for Ingress implementation
// (routing rules and bakend configuration) to its current "official"
// value. Invoked on TemplateConfig deletion, only needed when devmode
// is activated for the controller.
func ResetIngressTmpl() {
	ingressTmpl = template.Must(template.New(ingTmplName).Funcs(vclFuncs).
		Parse(ingTmplSrc))
}

// SetIngressTmpl parses src as a text/template, using the FuncMap
// defined for the ingress template, which is used to generate VCL for
// Ingress implementaion -- routing rules and backend configuration,
// including configuration set for the BackendConfig custom
// resource. On success, the ingress template is replaced. If the
// parse fails, then the error is returned and the shard template is
// unchanged.
//
// Only used when devmode is activated for the controller.
func SetIngressTmpl(src string) error {
	newTmpl, err := template.New(ingTmplName).Funcs(vclFuncs).Parse(src)
	if err != nil {
		return err
	}
	ingressTmpl = newTmpl
	return nil
}
