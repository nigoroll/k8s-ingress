/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"reflect"
	"testing"
)

var coffeeSvc3 = Service{
	Name: "coffee-svc",
	Addresses: []Address{
		{
			IP:   "192.0.2.4",
			Port: 80,
		},
		{
			IP:   "192.0.2.5",
			Port: 80,
		},
		{
			IP:   "192.0.2.6",
			Port: 80,
		},
	},
}

var barSpec2 = Spec{
	DefaultService: Service{},
	Rules: []Rule{
		{
			Host: "cafe.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/tea",
					Type: PathPrefix,
				}: teaSvc,
				{
					Path: "/coffee",
					Type: PathPrefix,
				}: coffeeSvc3,
			},
		},
		{
			Host: "whiskey.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extWhiskeySvc,
			},
		},
		{
			Host: "vodka.example.com",
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extVodkaSvc,
			},
		},
	},
	IntSvcs: map[string]Service{
		"tea-svc":    teaSvc,
		"coffee-svc": coffeeSvc3,
	},
	ExtSvcs: map[string]Service{
		"whiskey-svc": extWhiskeySvc,
		"vodka-svc":   extVodkaSvc,
	},
}

func TestDeepHash(t *testing.T) {
	if barSpec.DeepHash() == barSpec2.DeepHash() {
		t.Errorf("DeepHash(): Distinct specs have equal hashes")
		if testing.Verbose() {
			t.Logf("spec1: %+v", barSpec)
			t.Logf("spec2: %+v", barSpec2)
			t.Logf("hash: %s", cafeSpec.DeepHash())
		}
	}
}

var teaSvcShuf = Service{
	Name: "tea-svc",
	Addresses: []Address{
		{
			PodNamespace: "default",
			PodName:      "tea-5798f99dc5-5wj8n",
			IP:           "192.0.2.3",
			Port:         80,
		},
		{
			PodNamespace: "default",
			PodName:      "tea-5798f99dc5-5wj8n",
			IP:           "192.0.2.1",
			Port:         80,
		},
		{
			PodNamespace: "default",
			PodName:      "tea-5798f99dc5-hn27l",
			IP:           "192.0.2.2",
			Port:         80,
		},
	},
}

var coffeeSvcShuf = Service{
	Name: "coffee-svc",
	Addresses: []Address{
		{
			PodNamespace: "default",
			PodName:      "coffee-6b9f5c47d7-l5zvl",
			IP:           "192.0.2.5",
			Port:         80,
		},
		{
			PodNamespace: "default",
			PodName:      "coffee-6b9f5c47d7-bdt68",
			IP:           "192.0.2.4",
			Port:         80,
		},
	},
}

var barSpecShuf = Spec{
	DefaultService: Service{},
	Rules: []Rule{
		{
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extVodkaSvc,
			},
			Host: "vodka.example.com",
		},
		{
			PathMap: map[PathKey]Service{
				{
					Path: "/coffee",
					Type: PathPrefix,
				}: coffeeSvcShuf,
				{
					Path: "/tea",
					Type: PathPrefix,
				}: teaSvcShuf,
			},
			Host: "cafe.example.com",
		},
		{
			PathMap: map[PathKey]Service{
				{
					Path: "/",
					Type: PathPrefix,
				}: extWhiskeySvc,
			},
			Host: "whiskey.example.com",
		},
	},
	ExtSvcs: map[string]Service{
		"vodka-svc":   extVodkaSvc,
		"whiskey-svc": extWhiskeySvc,
	},
	IntSvcs: map[string]Service{
		"coffee-svc": coffeeSvcShuf,
		"tea-svc":    teaSvcShuf,
	},
}

func TestCanoncial(t *testing.T) {
	canonBar := barSpec.Canonical()
	canonShuf := barSpecShuf.Canonical()
	if !reflect.DeepEqual(canonBar, canonShuf) {
		t.Error("Canonical(): Equivalent VCL specs not deeply equal")
		if testing.Verbose() {
			t.Log("Canonical bar:", canonBar)
			t.Log("Canonical shuffled bar:", canonShuf)
		}
	}
	if canonBar.DeepHash() != canonShuf.DeepHash() {
		t.Error("Canonical(): Unequal hashes for equivalent specs")
		if testing.Verbose() {
			t.Logf("spec1 canonical: %+v", canonBar)
			t.Logf("spec1 hash: %s", canonBar.DeepHash())
			t.Logf("spec2 canonical: %+v", canonShuf)
			t.Logf("spec2 hash: %s", canonShuf.DeepHash())
		}
	}
}
