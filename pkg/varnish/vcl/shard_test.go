/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"bytes"
	_ "io/ioutil"
	"testing"
)

var varnishCluster = ShardCluster{
	Nodes: []Service{
		{
			Name: "viking-service",
			Addresses: []Address{{
				PodNamespace: "default",
				PodName:      "varnish-8445d4f7f-z2b9p",
				IP:           "172.17.0.12",
				Port:         80,
			}},
		},
		{
			Name: "viking-service",
			Addresses: []Address{{
				IP:   "172.17.0.13",
				Port: 80,
			}},
		},
		{
			Name: "viking-service",
			Addresses: []Address{{
				PodNamespace: "default",
				PodName:      "varnish-8445d4f7f-ldljf",
				IP:           "172.17.0.14",
				Port:         80,
			}},
		},
	},
	Probe: &Probe{
		Timeout:   "2s",
		Interval:  "5s",
		Initial:   "2",
		Window:    "8",
		Threshold: "3",
	},
	MaxSecondaryTTL: "5m",
	Rules:           []ShardRule{{}},
}

func TestShardTemplate(t *testing.T) {
	templateTest(t, shardTmpl, varnishCluster, "shard.golden")
}

func TestPrimaryOnlyShardTemplate(t *testing.T) {
	varnishCluster.Rules[0].PrimaryOnly = true
	templateTest(t, shardTmpl, varnishCluster, "primaryonly_shard.golden")
}

func TestGetSrc(t *testing.T) {
	gold := "ingress_shard.golden"
	cafeSpec.ShardCluster = varnishCluster
	cafeSpec.ShardCluster.Rules[0].PrimaryOnly = false
	src, err := cafeSpec.GetSrc()
	if err != nil {
		t.Error("Spec.GetSrc():", err)
		return
	}
	ok, err := cmpGold([]byte(src), gold)
	if err != nil {
		t.Fatalf("Reading %s: %v", gold, err)
	}
	if !ok {
		t.Errorf("Generated VCL does not match gold file: %s", gold)
		if testing.Verbose() {
			buf := bytes.NewBufferString(src)
			if msg, err := diffGold(*buf, gold); err == nil {
				t.Logf("diff:\n%s", msg)
			} else {
				t.Logf("Generated:\n%s", src)
			}
		}
	}
}

func TestShardByURL(t *testing.T) {
	varnishCluster.Rules[0].PrimaryOnly = true
	varnishCluster.Rules[0].By = URL
	templateTest(t, shardTmpl, varnishCluster, "shard_by_url.golden")
}

func TestShardByKey(t *testing.T) {
	varnishCluster.Rules[0].PrimaryOnly = true
	varnishCluster.Rules[0].By = Key
	varnishCluster.Rules[0].Key = "req.http.Host"
	templateTest(t, shardTmpl, varnishCluster, "shard_by_key.golden")
}

func TestShardByDigest(t *testing.T) {
	varnishCluster.Rules[0].PrimaryOnly = true
	varnishCluster.Rules[0].By = Blob
	varnishCluster.Rules[0].Key = "req.http.Host"
	varnishCluster.Rules[0].Algo = Sha3_512
	templateTest(t, shardTmpl, varnishCluster, "shard_by_digest.golden")
}

func TestShardConditions(t *testing.T) {
	varnishCluster.Rules[0].PrimaryOnly = true
	varnishCluster.Rules[0].By = Blob
	varnishCluster.Rules[0].Key = "req.http.Host"
	varnishCluster.Rules[0].Algo = Sha3_512
	varnishCluster.Rules[0].Conditions = []Condition{
		{
			Comparand: "req.url",
			Compare:   Prefix,
			Values:    []string{"/foo/"},
		},
	}
	varnishCluster.Rules = append(varnishCluster.Rules, ShardRule{})
	templateTest(t, shardTmpl, varnishCluster, "shard_conditions.golden")
}

func TestShardByCookie(t *testing.T) {
	varnishCluster.Rules[0].PrimaryOnly = true
	varnishCluster.Rules[0].By = Cookie
	varnishCluster.Rules[0].Key = "foobar"
	templateTest(t, shardTmpl, varnishCluster, "shard_by_cookie.golden")
}

func TestShardByCookieDefault(t *testing.T) {
	varnishCluster.Rules = []ShardRule{{
		PrimaryOnly: true,
		By:          Cookie,
		Key:         "bazquux",
		DefaultKey:  "defaultKey",
		Conditions:  []Condition{},
	}}
	templateTest(t, shardTmpl, varnishCluster,
		"shard_by_cookie_default.golden")
}
