
import std;
import directors;
import blob;
import blobdigest;
import taskvar;
import re2;
import selector;

probe vk8s_probe_varnish {
	.request = "HEAD /vk8s_cluster_health HTTP/1.1"
	           "Host: vk8s_cluster"
	           "Connection: close";
	.timeout = 2s;
	.interval = 5s;
	.initial = 2;
	.window = 8;
	.threshold = 3;
}

backend vk8s_default_varnish-8445d4f7f-z2b9p {
	.host = "172.17.0.12";
	.port = "80";
	.probe = vk8s_probe_varnish;
}

backend vk8s__ {
	.host = "172.17.0.13";
	.port = "80";
	.probe = vk8s_probe_varnish;
}

backend vk8s_default_varnish-8445d4f7f-ldljf {
	.host = "172.17.0.14";
	.port = "80";
	.probe = vk8s_probe_varnish;
}

acl vk8s_cluster_acl {
	"172.17.0.12";
	"172.17.0.13";
	"172.17.0.14";
}

sub vcl_init {
	new vk8s_cluster_param = directors.shard_param();
	new vk8s_cluster = directors.shard();
	vk8s_cluster.associate(vk8s_cluster_param.use());
	vk8s_cluster.add_backend(vk8s_default_varnish-8445d4f7f-z2b9p);
	vk8s_cluster.add_backend(vk8s__);
	vk8s_cluster.add_backend(vk8s_default_varnish-8445d4f7f-ldljf);
	vk8s_cluster.reconfigure();
	new vk8s_cluster_forward = taskvar.bool();
	new vk8s_cluster_primary = taskvar.backend();
	new vk8s_shard_cookie_0 =
		 re2.regex("\bfoobar\s*=\s*([^,;[:space:]]+)");
	new vk8s_selfshard_cond_0_0 = selector.set(case_sensitive=false);
	vk8s_selfshard_cond_0_0.add("/foo/");
}

sub vcl_recv {
	if (vk8s_selfshard_cond_0_0.hasprefix(req.url)) {
	
		set req.http.VK8S-Shard-Key = "";
		if (vk8s_shard_cookie_0.match(req.http.Cookie)) {
			set req.http.VK8S-Shard-Key
				= vk8s_shard_cookie_0.backref(1);
		}
		vk8s_cluster_primary.set(vk8s_cluster.backend(resolve=NOW, by=KEY, key=vk8s_cluster.key(req.http.VK8S-Shard-Key)));
		if (remote.ip !~ vk8s_cluster_acl
		    && "" + vk8s_cluster_primary.get() != server.identity) {
			set req.backend_hint = vk8s_cluster_primary.get();
			return (pipe);
		}
	} else
	if (remote.ip ~ vk8s_cluster_acl) {
		if (req.http.Host == "vk8s_cluster") {
			if (req.url == "/vk8s_cluster_health") {
				return (synth(200));
			}
			return (synth(404));
		}

		# prevent deadlock for accidental cyclic requests
		set req.hash_ignore_busy = true;

		# if we're async, don't deliver stale
		if (req.http.VK8S-Is-Bgfetch == "true") {
			set req.grace = 0s;
		}

		return (hash);
	}
}

sub vk8s_cluster_fetch {
	if (bereq.retries > 0 
	    || bereq.uncacheable
	    || remote.ip ~ vk8s_cluster_acl
	    || "" + vk8s_cluster.backend(resolve=NOW) == server.identity) {
		return;
	}
	
	    
	    
		
		vk8s_cluster_param.set();
		vk8s_cluster_forward.set(true);
		

	if (vk8s_cluster_forward.get(fallback=false)) {
		set bereq.backend = vk8s_cluster.backend(resolve=LAZY);
		set bereq.http.VK8S-Is-Bgfetch = bereq.is_bgfetch;
		return (fetch);
	}
}

sub vcl_backend_fetch {
	call vk8s_cluster_fetch;
}

sub vcl_backend_response {
	if (bereq.backend == vk8s_cluster.backend(resolve=LAZY)) {
	        if (beresp.http.VK8S-Cluster-TTL) {
		        set beresp.ttl = std.duration(
                	    beresp.http.VK8S-Cluster-TTL + "s", 1s);
		        if (beresp.ttl > 5m) {
			        set beresp.ttl = 5m;
		        }
		        unset beresp.http.VK8S-Cluster-TTL;
	        }
                else {
			set beresp.uncacheable = true;
		}
		return (deliver);
	}
}

sub vcl_backend_error {
	if (bereq.backend == vk8s_cluster.backend(resolve=LAZY)) {
		return (deliver);
	}
}

sub vcl_deliver {
	unset resp.http.VK8S-Cluster-TTL;
	if (remote.ip ~ vk8s_cluster_acl && ! vk8s_cluster_primary.defined()) {
		if (! obj.uncacheable) {
			set resp.http.VK8S-Cluster-TTL = obj.ttl;
		}
		return (deliver);
	}
}
