/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package vcl

import (
	"fmt"
	"text/template"
)

const reqDispTmplSrc = `
import re2;
import selector;

{{range $didx, $d := .Dispositions -}}
{{range $cidx, $c := .Conditions -}}
{{if reqNeedsMatcher $c -}}
sub vcl_init {
	new {{reqObj $didx $cidx}} = {{vmod $c.Compare}}.set({{reqFlags $c}});
	{{- range $val := $c.Values}}
	{{reqObj $didx $cidx}}.add("{{$val}}");
	{{- end}}
	{{reqObj $didx $cidx}}.compile();
}

{{end -}}
{{- end}}
{{- end}}
sub vcl_recv {
	{{- range $didx, $d := .Dispositions}}
	if (
	    {{- range $cidx, $cond := .Conditions}}
	    {{- if ne $cidx 0}} &&
            {{end}}
	    {{- if .Negate}}! {{end}}
	    {{- if reqNeedsMatcher $cond}}
	    {{- reqObj $didx $cidx}}.{{match .Compare}}({{.Comparand}})
	    {{- else if exists .Compare}}
	    {{- .Comparand}}
	    {{- else}}
            {{- .Comparand}} {{cmpRelation .Compare .Negate}} {{value $cond}}
	    {{- end}}
	    {{- end -}}
	   ) {
		return (
			{{- with .Disposition}}
			{{- if eq .Action "synth"}}synth({{.Status}}
				{{- if .Reason}}, "{{.Reason}}"{{end -}})
			{{- else}}{{.Action}}
			{{- end}}
			{{- end -}}
		       );
	}
	{{- end}}
	return (hash);
}
`

const reqDispTmplName = "request-disposition"

func reqNeedsMatcher(cond Condition) bool {
	switch cond.Compare {
	case Match, Prefix:
		return true
	case Exists, Greater, GreaterEqual, Less, LessEqual:
		return false
	}
	if cond.Count != nil || len(cond.Values) == 1 {
		return false
	}
	return true
}

func reqFlags(cond Condition) string {
	return matcherFlags(cond.Compare != Match, cond.MatchFlags)
}

func reqValue(cond Condition) string {
	if cond.Count != nil {
		return fmt.Sprintf("%d", *cond.Count)
	}
	return `"` + cond.Values[0] + `"`
}

var reqDispFuncs = template.FuncMap{
	"exists":   func(cmp CompareType) bool { return cmp == Exists },
	"match":    func(cmp CompareType) string { return match(cmp) },
	"value":    func(cond Condition) string { return reqValue(cond) },
	"vmod":     func(cmp CompareType) string { return vmod(cmp) },
	"reqFlags": func(cond Condition) string { return reqFlags(cond) },
	"cmpRelation": func(cmp CompareType, negate bool) string {
		return cmpRelation(cmp, negate)
	},
	"reqNeedsMatcher": func(cond Condition) bool {
		return reqNeedsMatcher(cond)
	},
	"reqObj": func(didx, cidx int) string {
		return fmt.Sprintf("vk8s_reqdisp_%d_%d", didx, cidx)
	},
}

var reqDispTmpl = template.Must(template.New(reqDispTmplName).
	Funcs(reqDispFuncs).Parse(reqDispTmplSrc))

// ResetReqDispTmpl sets the VCL template for the VarnishConfig
// reqDisp feature to its current "official" value. Invoked on
// TemplateConfig deletion, only needed when devmode is activated for
// the controller.
func ResetReqDispTmpl() {
	reqDispTmpl = template.Must(template.New(reqDispTmplName).
		Funcs(reqDispFuncs).Parse(reqDispTmplSrc))
}

// SetReqDispTmpl parses src as a text/template, using the FuncMap
// defined for the reqDisp template, which is used to generate VCL for
// the VarnishConfig reqDisp feature. On success, the reqDisp template
// is replaced. If the parse fails, then the error is returned and the
// reqDisp template is unchanged.
//
// Only used when devmode is activated for the controller.
func SetReqDispTmpl(src string) error {
	newTmpl, err := template.New(reqDispTmplName).Funcs(reqDispFuncs).
		Parse(src)
	if err != nil {
		return err
	}
	reqDispTmpl = newTmpl
	return nil
}
