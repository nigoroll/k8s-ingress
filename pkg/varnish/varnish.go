/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

// Package varnish encapsulates interaction with Varnish instances to
// transform desired states from Ingress and VarnishConfig configs to
// the actual state of the cluster. Only this package imports
// varnishapi/pkg/admin to interact with the CLI of each Varnish
// instance.
package varnish

import (
	"fmt"
	"io"
	"net"
	"reflect"
	"regexp"
	"strings"
	"sync"
	"time"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/interfaces"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/update"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish/vcl"
	"code.uplex.de/uplex-varnish/varnishapi/pkg/admin"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

const (
	regularLabel   = "vk8s_regular"
	readinessLabel = "vk8s_configured"
	readyCfg       = "vk8s_ok"
	notAvailCfg    = "vk8s_notavailable"
	ingressPrefix  = "vk8s_ing_"
)

// XXX make admTimeout configurable
var (
	nonAlNum   = regexp.MustCompile("[^[:alnum:]]+")
	admTimeout = time.Second * 10
)

// AdmError encapsulates an error encountered for an individual
// Varnish instance, and satisfies the Error interface.
type AdmError struct {
	inst string
	err  error
}

// Error returns an error meesage for an error encountered at a
// Varnish instance, identifying the instance by its Pod
// namespace/name if known, and its Endpoint address (internal IP) and
// admin port.
func (vadmErr AdmError) Error() string {
	return fmt.Sprintf("%s: %v", vadmErr.inst, vadmErr.err)
}

// AdmErrors is a collection of errors encountered at Varnish
// instances. Most attempts to sync the state of Varnish instances do
// not break off at the first error; the attempt is repeated for each
// instance in a cluster, collecting error information along the way.
// This object contains error information for each instance in a
// cluster that failed to sync. The type satisifies the Error
// interface.
type AdmErrors []AdmError

// Error returns an error message that includes errors for each
// instance in a Varnish cluster that failed a sync operation, where
// each instance is identified by it Endpoint (internal IP) and admin
// port.
func (vadmErrs AdmErrors) Error() string {
	var sb strings.Builder
	sb.WriteRune('[')
	for _, err := range vadmErrs {
		sb.WriteRune('{')
		sb.WriteString(err.Error())
		sb.WriteRune('}')
	}
	sb.WriteRune(']')
	return sb.String()
}

func (vadmErrs AdmErrors) status() update.Status {
	if vadmErrs == nil {
		return update.MakeSuccess("")
	}
	hasstatus, success, recoverable := false, false, false
	for _, e := range vadmErrs {
		status, ok := e.err.(update.Status)
		if !ok {
			continue
		}
		hasstatus = true
		switch status.Type {
		case update.Fatal:
			return update.MakeFatal("%v", vadmErrs)
		case update.Incomplete:
			return update.MakeIncomplete("%v", vadmErrs)
		case update.Recoverable:
			recoverable = true
		case update.Success:
			success = true
		}
	}
	if !hasstatus || recoverable {
		return update.MakeRecoverable("%v", vadmErrs)
	}
	if success {
		return update.MakeSuccess("%v", vadmErrs)
	}
	return update.MakeNoop("%v", vadmErrs)
}

func errStatus(err error) update.Status {
	status, ok := err.(update.Status)
	if ok {
		return status
	}
	errs, ok := err.(AdmErrors)
	if !ok {
		if err != nil {
			return update.MakeRecoverable("%v", err)
		}
		return update.MakeSuccess("")
	}
	return errs.status()
}

// Meta encapsulates meta-data for the resource types that enter into
// a Varnish configuration: Ingress, VarnishConfig and BackendConfig.
//
//	Key: namespace/name
//	UID: UID field from the resource meta-data
//	Ver: ResourceVersion field from the resource meta-data
type Meta struct {
	Key string
	UID string
	Ver string
}

type vclSpec struct {
	spec vcl.Spec
	ings map[string]Meta
	vcfg Meta
	bcfg map[string]Meta
}

func (spec vclSpec) configName() string {
	name := fmt.Sprint(ingressPrefix, spec.spec.Canonical().DeepHash())
	return nonAlNum.ReplaceAllLiteralString(name, "_")
}

type varnishInst struct {
	ns        string
	name      string
	addr      string
	admSecret *[]byte
	Banner    string
	admMtx    *sync.Mutex
}

func (inst varnishInst) String() string {
	if inst.ns != "" && inst.name != "" {
		return fmt.Sprintf("%s/%s(%s)", inst.ns, inst.name, inst.addr)
	}
	return inst.addr
}

type varnishSvc struct {
	instances []*varnishInst
	spec      *vclSpec
	secrName  string
	cfgLoaded bool
}

// Controller encapsulates information about each Varnish
// cluster deployed as Ingress implementations in the cluster, and
// their current states.
type Controller struct {
	log      *logrus.Logger
	svcEvt   interfaces.SvcEventGenerator
	svcs     map[string]*varnishSvc
	secrets  map[string]*[]byte
	wg       *sync.WaitGroup
	monIntvl time.Duration
}

// NewVarnishController returns an instance of Controller.
//
//	log: logger object initialized at startup
func NewVarnishController(
	log *logrus.Logger,
	monIntvl time.Duration,
) *Controller {
	initMetrics()
	return &Controller{
		svcs:     make(map[string]*varnishSvc),
		secrets:  make(map[string]*[]byte),
		log:      log,
		monIntvl: monIntvl,
		wg:       new(sync.WaitGroup),
	}
}

// EvtGenerator sets the object that implements interface
// SvcEventGenerator, and will be used by the monitor goroutine to
// generate Events for Varnish Services.
func (vc *Controller) EvtGenerator(svcEvt interfaces.SvcEventGenerator) {
	vc.svcEvt = svcEvt
}

// HasVarnishSvc returns true iff the configuration of the service
// identified by svcKey has been specified for the Varnish controller.
func (vc *Controller) HasVarnishSvc(svcKey string) bool {
	_, ok := vc.svcs[svcKey]
	return ok
}

// Start initiates the Varnish controller and starts the monitor
// goroutine.
func (vc *Controller) Start() {
	fmt.Printf("Varnish controller logging at level: %s\n", vc.log.Level)
	go vc.monitor(vc.monIntvl)
}

func (vc *Controller) updateVarnishInstance(inst *varnishInst, cfgName string,
	vclSrc string, metrics *instanceMetrics) error {

	vc.log.Infof("Update Varnish instance at %s", inst)
	vc.log.Tracef("Varnish instance %s: %+v", inst, *inst)
	if inst.admSecret == nil {
		return fmt.Errorf("No known admin secret")
	}
	inst.admMtx.Lock()
	defer inst.admMtx.Unlock()
	vc.wg.Add(1)
	defer vc.wg.Done()

	vc.log.Tracef("Connect to %s, timeout=%v", inst, admTimeout)
	timer := prometheus.NewTimer(metrics.connectLatency)
	adm, err := admin.Dial(inst.addr, *inst.admSecret, admTimeout)
	timer.ObserveDuration()
	if err != nil {
		metrics.connectFails.Inc()
		return err
	}
	defer adm.Close()
	inst.Banner = adm.Banner
	vc.log.Infof("Connected to Varnish admin endpoint at %s", inst)

	loaded, labelled, ready := false, false, false
	vc.log.Tracef("List VCLs at %s", inst)
	vcls, err := adm.VCLList()
	if err != nil {
		return err
	}
	vc.log.Tracef("VCL List at %s: %+v", inst, vcls)
	for _, vcl := range vcls {
		if vcl.Name == cfgName {
			loaded = true
		}
		if vcl.LabelVCL == cfgName &&
			vcl.Name == regularLabel {
			labelled = true
		}
		if vcl.LabelVCL == readyCfg &&
			vcl.Name == readinessLabel {
			ready = true
		}
	}

	if loaded {
		vc.log.Infof("Config %s already loaded at instance %s", cfgName,
			inst)
	} else {
		vc.log.Tracef("Load config %s at %s", cfgName, inst)
		timer = prometheus.NewTimer(metrics.vclLoadLatency)
		err = adm.VCLInline(cfgName, vclSrc)
		timer.ObserveDuration()
		if err != nil {
			vc.log.Tracef("Error loading config %s at %s: %v",
				cfgName, inst, err)
			metrics.vclLoadErrs.Inc()
			if resp, ok := err.(admin.UnexpectedResponse); ok {
				if resp.Response.Code < admin.OK {
					return update.MakeFatal("%v", resp)
				}
			}
			return err
		}
		metrics.vclLoads.Inc()
		vc.log.Infof("Loaded config %s at Varnish endpoint %s", cfgName,
			inst)
	}

	if labelled {
		vc.log.Infof("Config %s already labelled as regular at %s",
			cfgName, inst)
	} else {
		vc.log.Tracef("Label config %s as %s at %s", cfgName,
			regularLabel, inst)
		err = adm.VCLLabel(regularLabel, cfgName)
		if err != nil {
			return err
		}
		vc.log.Infof("Labeled config %s as %s at Varnish endpoint %s",
			cfgName, regularLabel, inst)
	}

	if ready {
		vc.log.Infof("Config %s already labelled as ready at %s",
			readyCfg, inst)
	} else {
		vc.log.Tracef("Label config %s as %s at %s", readyCfg,
			readinessLabel, inst)
		err = adm.VCLLabel(readinessLabel, readyCfg)
		if err != nil {
			return err
		}
		vc.log.Infof("Labeled config %s as %s at Varnish endpoint %s",
			readyCfg, readinessLabel, inst)
	}
	return nil
}

func (vc *Controller) updateVarnishSvc(name string) error {
	svc, exists := vc.svcs[name]
	if !exists || svc == nil {
		return fmt.Errorf("No known Varnish Service %s", name)
	}
	vc.log.Tracef("Update Varnish svc %s: config=%+v", name, *svc)
	svc.cfgLoaded = false
	if svc.secrName == "" {
		return update.MakeIncomplete(
			"No known admin secret for Varnish Service %s", name)
	}
	if svc.spec == nil {
		return update.MakeNoop(
			"Update Varnish Service %s: Currently no Ingress "+
				"defined", name)
	}

	vclSrc, err := svc.spec.spec.GetSrc()
	if err != nil {
		return update.MakeFatal("%v", err)
	}
	cfgName := svc.spec.configName()

	vc.log.Infof("Update Varnish instances: load config %s", cfgName)
	vc.log.Tracef("Config %s source: %s", cfgName, vclSrc)
	var errs AdmErrors
	for _, inst := range svc.instances {
		if inst == nil {
			vc.log.Errorf("Instance object is nil")
			continue
		}
		metrics := getInstanceMetrics(inst.addr)
		metrics.updates.Inc()
		if e := vc.updateVarnishInstance(inst, cfgName, vclSrc,
			metrics); e != nil {

			admErr := AdmError{inst: inst.String(), err: e}
			errs = append(errs, admErr)
			metrics.updateErrs.Inc()
			continue
		}
	}
	if len(errs) == 0 {
		svc.cfgLoaded = true
		return nil
	}
	return errs
}

// Label cfg as lbl at Varnish instance inst. If mayClose is true, then
// losing the admin connection is not an error (Varnish may be
// shutting down).
func (vc *Controller) setCfgLabel(inst *varnishInst, cfg, lbl string,
	mayClose bool) error {

	if inst.admSecret == nil {
		return AdmError{
			inst: inst.String(),
			err:  fmt.Errorf("No known admin secret"),
		}
	}
	metrics := getInstanceMetrics(inst.addr)
	inst.admMtx.Lock()
	defer inst.admMtx.Unlock()
	vc.wg.Add(1)
	defer vc.wg.Done()

	vc.log.Tracef("Connect to %s, timeout=%v", inst, admTimeout)
	timer := prometheus.NewTimer(metrics.connectLatency)
	adm, err := admin.Dial(inst.addr, *inst.admSecret, admTimeout)
	timer.ObserveDuration()
	if err != nil {
		if mayClose {
			vc.log.Warnf("Could not connect to %s: %v", inst, err)
			return nil
		}
		metrics.connectFails.Inc()
		return AdmError{inst: inst.String(), err: err}
	}
	defer adm.Close()
	inst.Banner = adm.Banner
	vc.log.Infof("Connected to Varnish admin endpoint at %s", inst)

	vc.log.Tracef("Set config %s to label %s at %s", inst, cfg, lbl)
	if err := adm.VCLLabel(lbl, cfg); err != nil {
		if err == io.EOF {
			if mayClose {
				vc.log.Warnf("Connection at EOF at %s", inst)
				return nil
			}
			return AdmError{inst: inst.String(), err: err}
		}
	}
	return nil
}

// Ignore permanent network errors on syncs for deletion -- the
// Varnish instance may already be gone.
func (vc *Controller) ignorePermNetErr(err error) bool {
	vc.log.Debugf("checking error type %T: %+v", err, err)
	neterr, ok := err.(net.Error)
	if !ok || !neterr.Temporary() {
		return false
	}
	vc.log.Warnf("Ignoring permanent network error: %+v", err)
	return true
}

// On Delete for a Varnish instance, we set it to the unready state.
func (vc *Controller) removeVarnishInstances(insts []*varnishInst) error {
	var errs AdmErrors

	for _, inst := range insts {
		// XXX health check for sharding config should fail
		if err := vc.setCfgLabel(inst, notAvailCfg, readinessLabel,
			true); err != nil {

			if vc.ignorePermNetErr(err) {
				continue
			}
			admErr := AdmError{inst: inst.String(), err: err}
			errs = append(errs, admErr)
			continue
		}
		instsGauge.Dec()
	}
	if len(errs) == 0 {
		return nil
	}
	return errs
}

func (vc *Controller) updateVarnishSvcAddrs(key string, addrs []vcl.Address,
	secrPtr *[]byte, loadVCL bool) error {

	var errs AdmErrors
	var newInsts, remInsts, keepInsts []*varnishInst

	svc, exists := vc.svcs[key]
	if !exists {
		// Shouldn't be possible
		return fmt.Errorf("No known Varnish Service %s", key)
	}

	updateAddrs := make(map[string]vcl.Address)
	prevAddrs := make(map[string]*varnishInst)
	for _, addr := range addrs {
		key := addr.Address()
		updateAddrs[key] = addr
	}
	for _, inst := range svc.instances {
		prevAddrs[inst.addr] = inst
	}
	for addr, vclAddr := range updateAddrs {
		inst, exists := prevAddrs[addr]
		if exists {
			keepInsts = append(keepInsts, inst)
			continue
		}
		newInst := &varnishInst{
			ns:        vclAddr.PodNamespace,
			name:      vclAddr.PodName,
			addr:      addr,
			admSecret: secrPtr,
			admMtx:    &sync.Mutex{},
		}
		newInsts = append(newInsts, newInst)
		instsGauge.Inc()
	}
	for addr, inst := range prevAddrs {
		_, exists := updateAddrs[addr]
		if !exists {
			remInsts = append(remInsts, inst)
		}
	}
	vc.log.Tracef("Varnish svc %s: keeping instances=%+v, "+
		"new instances=%+v, removing instances=%+v", key, keepInsts,
		newInsts, remInsts)
	svc.instances = append(keepInsts, newInsts...)

	for _, inst := range remInsts {
		vc.log.Tracef("Varnish svc %s setting to not ready: %+v", key,
			inst)
		if err := vc.setCfgLabel(inst, notAvailCfg, readinessLabel,
			true); err != nil {

			admErr := AdmError{inst: inst.String(), err: err}
			errs = append(errs, admErr)
			continue
		}
		instsGauge.Dec()
	}
	vc.log.Tracef("Varnish svc %s config: %+v", key, *svc)

	if loadVCL {
		vc.log.Tracef("Varnish svc %s: load VCL", key)
		updateErrs := vc.updateVarnishSvc(key)
		if updateErrs != nil {
			vadmErrs, ok := updateErrs.(AdmErrors)
			if ok {
				errs = append(errs, vadmErrs...)
			} else {
				return updateErrs
			}
		}
	}
	if len(errs) == 0 {
		return nil
	}
	return errs
}

// AddOrUpdateVarnishSvc causes a sync for the Varnish Service
// identified by namespace/name key.
//
//	addrs: list of admin addresses for instances in the Service
//	       (internal IPs and admin ports)
//	secrName: namespace/name of the admin secret to use for the
//	          Service
//	loadVCL: true if the VCL config for the Service should be
//	         reloaded
func (vc *Controller) AddOrUpdateVarnishSvc(
	key string,
	addrs []vcl.Address,
	secrName string,
	loadVCL bool,
) update.Status {
	var secrPtr *[]byte
	svc, svcExists := vc.svcs[key]
	if !svcExists {
		var instances []*varnishInst
		svc = &varnishSvc{}
		for _, addr := range addrs {
			admAddr := addr.Address()
			instance := &varnishInst{
				ns:     addr.PodNamespace,
				name:   addr.PodName,
				addr:   admAddr,
				admMtx: &sync.Mutex{},
			}
			vc.log.Tracef("Varnish svc %s: creating instance %+v",
				key, *instance)
			instances = append(instances, instance)
			instsGauge.Inc()
		}
		svc.instances = instances
		vc.svcs[key] = svc
		svcsGauge.Inc()
		vc.log.Tracef("Varnish svc %s: created config", key)
	}
	vc.log.Tracef("Varnish svc %s config: %+v", key, svc)

	svc.secrName = secrName
	if _, exists := vc.secrets[secrName]; exists {
		secrPtr = vc.secrets[secrName]
	} else {
		secrPtr = nil
	}
	for _, inst := range svc.instances {
		inst.admSecret = secrPtr
	}
	vc.log.Tracef("Varnish svc %s: updated instance with secret %s", key,
		secrName)

	vc.log.Tracef("Update Varnish svc %s: addrs=%+v secret=%s reloadVCL=%v",
		key, addrs, secrName, loadVCL)
	if secrPtr != nil {
		vc.log.Tracef("secret contents = %v", *secrPtr)
	} else {
		vc.log.Trace("secret is nil")
	}
	return errStatus(vc.updateVarnishSvcAddrs(key, addrs, secrPtr, loadVCL))
}

// DeleteVarnishSvc is called on the Delete event for the Varnish
// Service identified by the namespace/name key. The Varnish instance
// is set to the unready state, and no further action is taken (other
// resources in the cluster may shut down the Varnish instances).
func (vc *Controller) DeleteVarnishSvc(key string) update.Status {
	vc.log.Trace("DeleteVarnishSvc: key=", key)
	svc, ok := vc.svcs[key]
	if !ok {
		return update.MakeNoop("No Varnish Service %s", key)
	}
	vc.log.Tracef("DeleteVarnishSvc: svc=%+v", svc)
	err := vc.removeVarnishInstances(svc.instances)
	vc.log.Tracef("DeleteVarnishSvc map before delete = %+v", vc.svcs)
	if err == nil {
		delete(vc.svcs, key)
		svcsGauge.Dec()
	}
	vc.log.Tracef("DeleteVarnishSvc map after delete = %+v", vc.svcs)
	return errStatus(err)
}

func (vc *Controller) updateBeGauges() {
	nBeSvcs := 0
	nBeEndps := 0
	for _, svc := range vc.svcs {
		if svc == nil || svc.spec == nil {
			continue
		}
		nBeSvcs += len(svc.spec.spec.IntSvcs)
		for _, beSvc := range svc.spec.spec.IntSvcs {
			nBeEndps += len(beSvc.Addresses)
		}
		nBeSvcs += len(svc.spec.spec.ExtSvcs)
		nBeEndps += len(svc.spec.spec.ExtSvcs)
	}
	beSvcsGauge.Set(float64(nBeSvcs))
	beEndpsGauge.Set(float64(nBeEndps))
}

// Update a Varnish Service to implement an configuration.
//
//	svcKey: namespace/name key for the Service
//	spec: VCL spec corresponding to the configuration
//	ingsMeta: Ingress meta-data
//	vcfgMeta: VarnishConfig meta-data
//	bcfgMeta: BackendConfig meta-data
func (vc *Controller) Update(
	svcKey string,
	spec vcl.Spec,
	addrs []vcl.Address,
	ingsMeta map[string]Meta,
	vcfgMeta Meta,
	bcfgMeta map[string]Meta,
) update.Status {
	var secrPtr *[]byte

	for key, svc := range spec.IntSvcs {
		if svc.Addresses == nil || len(svc.Addresses) == 0 {
			return update.MakeIncomplete(
				"Backend service %s: no addresses specified",
				key)
		}
	}
	svc, exists := vc.svcs[svcKey]
	if !exists {
		svc = &varnishSvc{
			instances: make([]*varnishInst, len(addrs)),
		}
		for i, addr := range addrs {
			svc.instances[i] = &varnishInst{
				ns:     addr.PodNamespace,
				name:   addr.PodName,
				addr:   addr.Address(),
				admMtx: &sync.Mutex{},
			}
		}
		vc.svcs[svcKey] = svc
		svcsGauge.Inc()
		vc.log.Infof("Added Varnish service definition %s", svcKey)
	}
	svc.cfgLoaded = false
	if svc.spec == nil {
		svc.spec = &vclSpec{}
	}
	svc.spec.spec = spec
	svc.spec.ings = ingsMeta
	svc.spec.vcfg = vcfgMeta
	svc.spec.bcfg = bcfgMeta
	vc.updateBeGauges()

	if len(svc.instances) == 0 {
		return update.MakeIncomplete(
			"Currently no known endpoints for Varnish service %s",
			svcKey)
	}

	if _, exists := vc.secrets[svc.secrName]; exists {
		secrPtr = vc.secrets[svc.secrName]
		for _, inst := range svc.instances {
			inst.admSecret = secrPtr
		}
	} else {
		secrPtr = nil
	}
	return errStatus(vc.updateVarnishSvcAddrs(svcKey, addrs, secrPtr, true))
}

// SetNotReady may be called on the Delete event on an Ingress, if no
// Ingresses remain that are to be implemented by a Varnish Service.
// The Service is set to the not configured state, by relabelling VCL so
// that the "configured" endpoint is not answered with status 200. Also
// set the "regular" label to the "not available" config.
func (vc *Controller) SetNotReady(svcKey string) error {
	svc, ok := vc.svcs[svcKey]
	if !ok {
		return fmt.Errorf("Set Varnish Service not ready: %s unknown",
			svcKey)
	}
	svc.spec = nil

	var errs AdmErrors
	for _, inst := range svc.instances {
		for _, label := range []string{readinessLabel, regularLabel} {
			if err := vc.setCfgLabel(inst, notAvailCfg, label,
				false); err != nil {

				if vc.ignorePermNetErr(err) {
					continue
				}
				admErr := AdmError{
					inst: inst.String(),
					err:  err,
				}
				errs = append(errs, admErr)
				continue
			}
		}
	}
	if len(errs) == 0 {
		return nil
	}
	return errs
}

// HasConfig returns true iff a configuration is already loaded for a
// Varnish Service (so a new sync attempt is not necessary).
//
//	svcKey: namespace/name key for the Varnish Service
//	spec: VCL specification derived from the configuration
//	ingsMeta: Ingress meta-data
//	vcfgMeta: VarnishConfig meta-data
//	bcfgMeta: BackendConfig meta-data
func (vc *Controller) HasConfig(svcKey string, addrs []vcl.Address,
	spec vcl.Spec, ingsMeta map[string]Meta, vcfgMeta Meta,
	bcfgMeta map[string]Meta) bool {

	svc, ok := vc.svcs[svcKey]
	if !ok {
		return false
	}
	if svc == nil {
		return false
	}
	if !svc.cfgLoaded {
		return false
	}
	if svc.spec == nil {
		return false
	}
	if svc.spec.ings == nil {
		return false
	}
	if len(ingsMeta) != len(svc.spec.ings) {
		return false
	}
	if svc.spec.bcfg == nil {
		return false
	}

	if len(addrs) != len(svc.instances) {
		return false
	}
	newAddrs := make(map[string]struct{})
	for _, addr := range addrs {
		a := addr.Address()
		newAddrs[a] = struct{}{}
	}
	curAddrs := make(map[string]struct{})
	for _, inst := range svc.instances {
		curAddrs[inst.addr] = struct{}{}
	}
	if !reflect.DeepEqual(newAddrs, curAddrs) {
		return false
	}

	if len(bcfgMeta) != len(svc.spec.bcfg) {
		return false
	}
	if vcfgMeta.Key != svc.spec.vcfg.Key ||
		vcfgMeta.UID != svc.spec.vcfg.UID ||
		vcfgMeta.Ver != svc.spec.vcfg.Ver {
		return false
	}
	for k, v := range ingsMeta {
		specIng, exists := svc.spec.ings[k]
		if !exists {
			return false
		}
		if specIng.Key != v.Key || specIng.UID != v.UID ||
			specIng.Ver != v.Ver {
			return false
		}
	}
	for k, v := range bcfgMeta {
		specBcfg, exists := svc.spec.bcfg[k]
		if !exists {
			return false
		}
		if specBcfg.Key != v.Key || specBcfg.UID != v.UID ||
			specBcfg.Ver != v.Ver {
			return false
		}
	}
	return reflect.DeepEqual(svc.spec.spec.Canonical(), spec.Canonical())
}

// SetAdmSecret stores the Secret data identified by the
// namespace/name key.
func (vc *Controller) SetAdmSecret(key string, secret []byte) {
	secr, exists := vc.secrets[key]
	if !exists {
		secretSlice := make([]byte, len(secret))
		secr = &secretSlice
		vc.secrets[key] = secr
		secretsGauge.Inc()
	}
	copy(*vc.secrets[key], secret)
}

// UpdateSvcForSecret associates the Secret identified by the
// namespace/name secretKey with the Varnish Service identified by the
// namespace/name svcKey. The Service is newly synced if necessary.
func (vc *Controller) UpdateSvcForSecret(
	svcKey, secretKey string,
) update.Status {
	secret, exists := vc.secrets[secretKey]
	if !exists {
		secretKey = ""
		secret = nil
	}
	svc, exists := vc.svcs[svcKey]
	if !exists {
		if secret == nil {
			return update.MakeNoop(
				"Neither Varnish Service %s nor secret %s found",
				svcKey, secretKey)
		}
		vc.log.Infof("Creating Varnish Service %s to set secret %s",
			svcKey, secretKey)
		svc = &varnishSvc{instances: make([]*varnishInst, 0)}
		vc.svcs[svcKey] = svc
		svcsGauge.Inc()
	}
	svc.secrName = secretKey

	for _, inst := range svc.instances {
		vc.log.Infof("Setting secret for instance %s", inst)
		inst.admSecret = secret
	}

	vc.log.Infof("Updating Service %s after setting secret %s", svcKey,
		secretKey)
	return errStatus(vc.updateVarnishSvc(svcKey))
}

// DeleteAdmSecret removes the secret identified by the namespace/name
// key.
func (vc *Controller) DeleteAdmSecret(name string) {
	_, exists := vc.secrets[name]
	if exists {
		delete(vc.secrets, name)
		secretsGauge.Dec()
	}
}

// Quit stops the Varnish controller.
func (vc *Controller) Quit() {
	vc.log.Info("Wait for admin interactions with Varnish instances to " +
		"finish")
	vc.wg.Wait()
}
